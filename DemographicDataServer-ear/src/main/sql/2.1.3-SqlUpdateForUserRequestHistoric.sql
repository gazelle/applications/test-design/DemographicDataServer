-- Set false by default
UPDATE dds_user_request_historic SET unlimited=false;

-- Set IP form Kereval unlimited
UPDATE dds_user_request_historic SET unlimited=true WHERE ip_address='62.212.122.29';
-- Set IP form portal unlimited
UPDATE dds_user_request_historic SET unlimited=true WHERE ip_address='91.121.85.146';
-- Set IP of servers hosted at irisa unlimited
UPDATE dds_user_request_historic SET unlimited=true WHERE ip_address like '131.254.209.%';