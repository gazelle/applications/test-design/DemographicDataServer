INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_documentation', 'https://gazelle.ihe.net/content/dds-user-manual');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v3_sender_timeout', '15000');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'NUMBER_OF_ITEMS_PER_PAGE', '20');

ALTER TABLE dds_country_code ADD  COLUMN  is_usable BOOLEAN DEFAULT FALSE ;
UPDATE dds_country_code SET is_usable = true WHERE id IN (select  distinct country_code_id from dds_last_name_country_code);
ALTER TABLE app_configuration ADD CONSTRAINT app_configuration_variable_key UNIQUE (variable);