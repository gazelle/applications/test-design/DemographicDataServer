INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_release_notes_url', 'https://gazelle.ihe.net/jira/browse/DDS#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_admin_title', 'Application Administrator');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_admin_email', 'anne-gaelle@ihe-europe.net');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_admin_name', 'Anne-Gaëlle Bergé');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'issue_tracker_url', 'https://gazelle.ihe.net/jira/browse/DDS');

UPDATE app_configuration SET value='https://gazelle.ihe.net/gazelle-documentation/Demographic-Data-Server/user.html' WHERE variable='application_documentation';