UPDATE dds_character_set_encoding SET hl7_charset = 'ASCII' where value = 'US-ASCII';
UPDATE dds_character_set_encoding SET hl7_charset = '8859/1' where value = 'ISO-8859-1';
UPDATE dds_character_set_encoding SET hl7_charset = '8859/2' where value = 'ISO-8859-2';
UPDATE dds_character_set_encoding SET hl7_charset = '8859/3' where value = 'ISO-8859-3';
UPDATE dds_character_set_encoding SET hl7_charset = '8859/4' where value = 'ISO-8859-4';
UPDATE dds_character_set_encoding SET hl7_charset = '8859/5' where value = 'ISO-8859-5';
UPDATE dds_character_set_encoding SET hl7_charset = '8859/6' where value = 'ISO-8859-6';
UPDATE dds_character_set_encoding SET hl7_charset = '8859/7' where value = 'ISO-8859-7';
UPDATE dds_character_set_encoding SET hl7_charset = '8859/8' where value = 'ISO-8859-8';
UPDATE dds_character_set_encoding SET hl7_charset = '8859/9' where value = 'ISO-8859-9';
UPDATE dds_character_set_encoding SET hl7_charset = 'ISO IR87' where value = 'ISO-2022-JP'; -- name it JIS X 0208-1990 either
UPDATE dds_character_set_encoding SET hl7_charset = 'GB 18030-2000' where value = 'GB18030'; -- complete name is GB 18030-2000
UPDATE dds_character_set_encoding SET hl7_charset = 'ASCII' where value = 'GB2312';--not supported by HL7 ?
UPDATE dds_character_set_encoding SET hl7_charset = 'ASCII' where value = 'ISO-2022-CN'; -- not supported by HL7 !,
UPDATE dds_character_set_encoding SET hl7_charset = 'KS X 1001' where value = 'EUC-KR';
UPDATE dds_character_set_encoding SET hl7_charset = 'BIG-5' where value = 'Big5';
UPDATE dds_character_set_encoding SET hl7_charset = 'UNICODE UTF-8' where value = 'UTF-8';
UPDATE dds_character_set_encoding SET hl7_charset = 'UNICODE UTF-16' where value = 'UTF-16';
UPDATE dds_character_set_encoding SET hl7_charset = 'UNICODE UTF-32' where value = 'UTF-32';
UPDATE dds_character_set_encoding SET hl7_charset = '8859/13' where value = 'ISO-8859-13'; -- ISO-8859-15 ?
-- add ISO-8859-15 
