delete from dds_patient_patient_address;
delete from dds_patient_address;
delete from dds_person_dds_other_name;
DELETE FROM dds_person_first_name_sex;
delete from dds_patient;
delete from dds_person;
delete from dds_user_ip_address;
delete from dds_user_request_historic;
update dds_web_services_information set last_request_time = null;
update dds_web_services_information set last_user_ip_id = 0;
UPDATE dds_web_services_information SET request_since_last_day = 0;
UPDATE dds_web_services_information SET total_request = 0;