-- http://docplayer.it/1552419-Piattaforma-affinity-domain-versione-2-0.html
update dds_country_code SET country_oid = '2.16.840.1.113883.2.9.4.3.2' where name = 'ITALY';

-- doc.brusafe.be/spaces/flyingpdf/pdfpageexport.action?pageId=1540100
update dds_country_code SET country_oid = '1.3.6.1.4.1.21297.100.1.1' where name = 'BELGIUM';

-- https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0ahUKEwj5zZqzl93bAhWJtBQKHcTrC8gQFggpMAA&url=https%3A%2F%2Fwww.nspop.dk%2Fdownload%2Fattachments%2F46583404%2FIFS0020%2520Svareksponeringsservice-laboratoriesvar%2520via%2520dokumentdeling%2520-%2520snitfladebeskrivelse.docx%3Fversion%3D1%26modificationDate%3D1488978828459%26api%3Dv2&usg=AOvVaw30BYP6VTufrFZKgkJae0Rq
update dds_country_code SET country_oid = '1.2.208.176.1.2' where name = 'DENMARK';
