--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: add_first_name_sex_alternate_spelling(text, integer, text); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.add_first_name_sex_alternate_spelling(text, integer, text) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
DECLARE
  b_id INTEGER ;
  c_id INTEGER ;
BEGIN
SELECT INTO b_id id FROM dds_first_name_sex WHERE value = initcap(lower($1)) and gender_id = $2;
IF b_id IS NULL THEN
INSERT INTO dds_first_name_sex (id, last_changed, last_modifier_id, value, gender_id) VALUES (nextval('dds_first_name_sex_id_seq'), NULL, NULL, initcap(lower($1)), $2);
END IF;
SELECT INTO c_id id FROM dds_first_name_alternate_spelling  WHERE first_name_sex_id = b_id and value = $3 ;
IF c_id IS NULL THEN
INSERT INTO dds_first_name_alternate_spelling (id, last_changed, last_modifier_id,  value, first_name_sex_id) SELECT  nextval('dds_first_name_alternate_spelling_id_seq'), 'now',  'epoiseau', $3, id from dds_first_name_sex where value = initcap(lower($1)) and gender_id = $2 ;
END IF ;

RETURN 1;
END;
$_$;


ALTER FUNCTION public.add_first_name_sex_alternate_spelling(text, integer, text) OWNER TO gazelle;

--
-- Name: add_first_name_sex_country(text, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.add_first_name_sex_country(text, integer, integer, integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$DECLARE
  b_id INTEGER ;
  c_id INTEGER ;
BEGIN
SELECT INTO b_id id FROM dds_first_name_sex WHERE value = $1 and gender_id = $2;
IF b_id IS NULL THEN
INSERT INTO dds_first_name_sex (id, last_changed, last_modifier_id, value, gender_id) VALUES (nextval('dds_first_name_sex_id_seq'), NULL, NULL, $1, $2);
END IF;
SELECT INTO c_id id FROM dds_first_name_country_code WHERE country_code_id = $3 and first_name_sex_id = b_id ;
IF c_id IS NULL THEN
INSERT INTO dds_first_name_country_code (id, last_changed, last_modifier_id, cumul, frequency, country_code_id, first_name_sex_id) SELECT  nextval('dds_first_name_country_code_id_seq'), 'now',  'epoiseau', 1, $4, $3, id from dds_first_name_sex where value = $1 and gender_id = $2 ;
END IF ;
update dds_first_name_country_code set cumul = (SELECT sum(frequency) from dds_first_name_country_code  a where a.country_code_id = $3 and a.id <= dds_first_name_country_code.id) where country_code_id = $3 ;

RETURN 1;
END;
$_$;


ALTER FUNCTION public.add_first_name_sex_country(text, integer, integer, integer) OWNER TO gazelle;

--
-- Name: FUNCTION add_first_name_sex_country(text, integer, integer, integer); Type: COMMENT; Schema: public; Owner: gazelle
--

COMMENT ON FUNCTION public.add_first_name_sex_country(text, integer, integer, integer) IS 'firstname, gender f=1 m=2, country code, frequency';


--
-- Name: add_last_name_alternate_spelling(text, text); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.add_last_name_alternate_spelling(text, text) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
DECLARE
  b_id INTEGER ;
  c_id INTEGER ;
BEGIN
SELECT INTO b_id id FROM dds_last_name WHERE value = initcap(lower($1)) ;
IF b_id IS NULL THEN
INSERT INTO dds_last_name (id, last_changed, last_modifier_id, value, gender_id) VALUES (nextval('dds_last_name_id_seq'), NULL, NULL, initcap(lower($1)), $2);
END IF;
SELECT INTO c_id id FROM dds_last_name_alternate_spelling  WHERE last_name_id = b_id and value = $2 ;
IF c_id IS NULL THEN
INSERT INTO dds_last_name_alternate_spelling (id, last_changed, last_modifier_id,  value, last_name_id) SELECT  nextval('dds_last_name_alternate_spelling_id_seq'), 'now',  'epoiseau', $2, id from dds_last_name where value = initcap(lower($1)) ;
END IF ;

RETURN 1;
END;
$_$;


ALTER FUNCTION public.add_last_name_alternate_spelling(text, text) OWNER TO gazelle;

--
-- Name: add_last_name_sex_country(text, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.add_last_name_sex_country(text, integer, integer, integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$ 
DECLARE
  b_id INTEGER ;
  c_id INTEGER ;
BEGIN
SELECT INTO b_id id FROM dds_last_name WHERE value = initcap(lower($1)) ;
IF b_id IS NULL THEN
INSERT INTO dds_last_name (id, last_changed, last_modifier_id, value, gender_id) VALUES (nextval('dds_last_name_id_seq'), NULL, NULL, initcap(lower($1)), $2);
END IF ;
SELECT INTO c_id id FROM dds_last_name_country_code WHERE country_code_id = $3 and last_name_id = b_id ;
IF c_id IS NULL THEN
INSERT INTO dds_last_name_country_code (id, last_changed, last_modifier_id, cumul, frequency, country_code_id, last_name_id) SELECT  nextval('dds_last_name_country_code_id_seq'), 'now',  'epoiseau', 1, $4, $3, id from dds_last_name where value = initcap(lower($1)) ;
ELSE
update dds_last_name_country_code set frequency = $4 where id = c_id ;
END IF ;
update dds_last_name_country_code set cumul = (SELECT sum(frequency) from dds_last_name_country_code  a where a.country_code_id = $3 and a.id <= dds_last_name_country_code.id) where country_code_id = $3 ;
return 1;
END;
$_$;


ALTER FUNCTION public.add_last_name_sex_country(text, integer, integer, integer) OWNER TO gazelle;

--
-- Name: comma_cat(text, text); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.comma_cat(text, text) RETURNS text
    LANGUAGE sql
    AS $_$select case WHEN $2 is null or $2 = '' THEN $1 WHEN $1 is null or $1 = '' THEN $2 ELSE $1 || ', ' || $2 END$_$;


ALTER FUNCTION public.comma_cat(text, text) OWNER TO gazelle;

--
-- Name: del_first_name_sex_entry(integer); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.del_first_name_sex_entry(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$ 
BEGIN
DELETE FROM dds_patient_patient_address where patient_id in (select id from dds_patient where person_dds_id  in ( SELECT id from dds_person where first_name_sex_id = $1 ) );
delete from dds_patient where person_dds_id  in ( SELECT id from dds_person where first_name_sex_id = $1 ) ;
DELETE FROM dds_person WHERE first_name_sex_id = $1 ;
DELETE FROM dds_first_name_country_code where first_name_sex_id = $1;
DELETE FROM dds_first_name_sex WHERE id=$1; 
return 1;
END;
$_$;


ALTER FUNCTION public.del_first_name_sex_entry(integer) OWNER TO gazelle;

--
-- Name: difference(text, text); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.difference(text, text) RETURNS integer
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/fuzzystrmatch', 'difference';


ALTER FUNCTION public.difference(text, text) OWNER TO gazelle;

--
-- Name: dmetaphone(text); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.dmetaphone(text) RETURNS text
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/fuzzystrmatch', 'dmetaphone';


ALTER FUNCTION public.dmetaphone(text) OWNER TO gazelle;

--
-- Name: dmetaphone_alt(text); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.dmetaphone_alt(text) RETURNS text
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/fuzzystrmatch', 'dmetaphone_alt';


ALTER FUNCTION public.dmetaphone_alt(text) OWNER TO gazelle;

--
-- Name: levenshtein(text, text); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.levenshtein(text, text) RETURNS integer
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/fuzzystrmatch', 'levenshtein';


ALTER FUNCTION public.levenshtein(text, text) OWNER TO gazelle;

--
-- Name: levenshtein(text, text, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.levenshtein(text, text, integer, integer, integer) RETURNS integer
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/fuzzystrmatch', 'levenshtein_with_costs';


ALTER FUNCTION public.levenshtein(text, text, integer, integer, integer) OWNER TO gazelle;

--
-- Name: metaphone(text, integer); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.metaphone(text, integer) RETURNS text
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/fuzzystrmatch', 'metaphone';


ALTER FUNCTION public.metaphone(text, integer) OWNER TO gazelle;

--
-- Name: soundex(text); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.soundex(text) RETURNS text
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/fuzzystrmatch', 'soundex';


ALTER FUNCTION public.soundex(text) OWNER TO gazelle;

--
-- Name: text_soundex(text); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.text_soundex(text) RETURNS text
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/fuzzystrmatch', 'soundex';


ALTER FUNCTION public.text_soundex(text) OWNER TO gazelle;

--
-- Name: list(text); Type: AGGREGATE; Schema: public; Owner: gazelle
--

CREATE AGGREGATE public.list(text) (
    SFUNC = public.comma_cat,
    STYPE = text,
    INITCOND = ''
);


ALTER AGGREGATE public.list(text) OWNER TO gazelle;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE public.app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_application_preference; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_application_preference (
    id integer NOT NULL,
    class_name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    preference_name character varying(64) NOT NULL,
    preference_value character varying(512) NOT NULL
);


ALTER TABLE public.cmn_application_preference OWNER TO gazelle;

--
-- Name: cmn_application_preference_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_application_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_application_preference_id_seq OWNER TO gazelle;

--
-- Name: cmn_event_tracking; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_event_tracking (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(255),
    login_id integer,
    object_class character varying(255) NOT NULL,
    object_id integer,
    type_event character varying(255)
);


ALTER TABLE public.cmn_event_tracking OWNER TO gazelle;

--
-- Name: cmn_event_tracking_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_event_tracking_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_event_tracking_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_number_of_results_per_page; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_number_of_results_per_page (
    id integer NOT NULL,
    number_of_results_per_page integer
);


ALTER TABLE public.cmn_number_of_results_per_page OWNER TO gazelle;

--
-- Name: cmn_number_of_results_per_page_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_number_of_results_per_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_number_of_results_per_page_id_seq OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_path_linking_a_document (
    id integer NOT NULL,
    comment character varying(255),
    path character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.cmn_path_linking_a_document OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_path_linking_a_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_path_linking_a_document_id_seq OWNER TO gazelle;

--
-- Name: dds_character_encoding; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_character_encoding (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    value character varying(255),
    last_modifier character varying(255)
);


ALTER TABLE public.dds_character_encoding OWNER TO gazelle;

--
-- Name: dds_character_encoding_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_character_encoding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_character_encoding_id_seq OWNER TO gazelle;

--
-- Name: dds_character_set_encoding; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_character_set_encoding (
    id integer NOT NULL,
    value character varying(255) NOT NULL,
    hl7_charset character varying(255)
);


ALTER TABLE public.dds_character_set_encoding OWNER TO gazelle;

--
-- Name: dds_character_set_encoding_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_character_set_encoding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_character_set_encoding_id_seq OWNER TO gazelle;

--
-- Name: dds_city; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_city (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    value character varying(255),
    last_modifier character varying(255)
);


ALTER TABLE public.dds_city OWNER TO gazelle;

--
-- Name: dds_city_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_city_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    value character varying(255)
);


ALTER TABLE public.dds_city_aud OWNER TO gazelle;

--
-- Name: dds_city_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_city_id_seq OWNER TO gazelle;

--
-- Name: dds_country_code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_country_code (
    id integer NOT NULL,
    ec boolean,
    flag_url character varying(255),
    country_code_iso character varying(2) NOT NULL,
    iso3 character varying(3),
    name character varying(80) NOT NULL,
    numcode integer,
    printable_name character varying(80) NOT NULL,
    is_last_name_sexed boolean,
    character_set_encoding_id_reference integer,
    generate_race boolean,
    generate_religion boolean,
    country_oid character varying(255),
    max_number_of_patient_middle_name integer,
    min_number_of_patient_middle_name integer,
    is_usable boolean DEFAULT false
);


ALTER TABLE public.dds_country_code OWNER TO gazelle;

--
-- Name: dds_country_code_character_set; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_country_code_character_set (
    character_set_encoding_id integer NOT NULL,
    country_code_id integer NOT NULL
);


ALTER TABLE public.dds_country_code_character_set OWNER TO gazelle;

--
-- Name: dds_country_code_language; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_country_code_language (
    country_code_id integer NOT NULL,
    language_id integer NOT NULL
);


ALTER TABLE public.dds_country_code_language OWNER TO gazelle;

--
-- Name: dds_first_name_alternate_spelling; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_first_name_alternate_spelling (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    value character varying(255),
    first_name_sex_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_first_name_alternate_spelling OWNER TO gazelle;

--
-- Name: dds_first_name_alternate_spelling_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_first_name_alternate_spelling_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_first_name_alternate_spelling_id_seq OWNER TO gazelle;

--
-- Name: dds_first_name_country_code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_first_name_country_code (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    cumul integer,
    frequency integer,
    country_code_id integer,
    first_name_sex_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_first_name_country_code OWNER TO gazelle;

--
-- Name: dds_first_name_country_code_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_first_name_country_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_first_name_country_code_id_seq OWNER TO gazelle;

--
-- Name: dds_first_name_sex; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_first_name_sex (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    value character varying(255),
    gender_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_first_name_sex OWNER TO gazelle;

--
-- Name: dds_first_name_sex_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_first_name_sex_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_first_name_sex_id_seq OWNER TO gazelle;

--
-- Name: dds_gender; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_gender (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(255),
    last_modifier character varying(255)
);


ALTER TABLE public.dds_gender OWNER TO gazelle;

--
-- Name: dds_gender_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_gender_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_gender_id_seq OWNER TO gazelle;

--
-- Name: dds_language; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_language (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(255),
    iso_639_1 character varying(255),
    iso_639_2 character varying(255),
    iso_639_3 character varying(255),
    native_name character varying(255),
    character_encoding_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_language OWNER TO gazelle;

--
-- Name: dds_language_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_language_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_language_id_seq OWNER TO gazelle;

--
-- Name: dds_last_name; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_last_name (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    value character varying(255),
    gender_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_last_name OWNER TO gazelle;

--
-- Name: dds_last_name_alternate_spelling; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_last_name_alternate_spelling (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    value character varying(255),
    last_name_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_last_name_alternate_spelling OWNER TO gazelle;

--
-- Name: dds_last_name_alternate_spelling_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_last_name_alternate_spelling_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_last_name_alternate_spelling_id_seq OWNER TO gazelle;

--
-- Name: dds_last_name_country_code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_last_name_country_code (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    cumul integer,
    frequency integer,
    country_code_id integer,
    last_name_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_last_name_country_code OWNER TO gazelle;

--
-- Name: dds_last_name_country_code_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_last_name_country_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_last_name_country_code_id_seq OWNER TO gazelle;

--
-- Name: dds_last_name_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_last_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_last_name_id_seq OWNER TO gazelle;

--
-- Name: dds_other_name; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_other_name (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    name_type character varying(255),
    first_name_sex_id integer,
    last_name_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_other_name OWNER TO gazelle;

--
-- Name: dds_other_name_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_other_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_other_name_id_seq OWNER TO gazelle;

--
-- Name: dds_patient; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_patient (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    creation_date timestamp without time zone,
    person_dds_id integer,
    national_patient_identifier character varying(255),
    country_id integer,
    dds_patient_identifier character varying(255),
    motherpatient_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_patient OWNER TO gazelle;

--
-- Name: dds_patient_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_patient_address (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    lattitude double precision,
    longitude double precision,
    number character varying(255),
    postal_code_id character varying(255),
    city_id integer,
    country_id integer,
    state_id integer,
    street_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_patient_address OWNER TO gazelle;

--
-- Name: dds_patient_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_patient_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_patient_address_id_seq OWNER TO gazelle;

--
-- Name: dds_patient_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_patient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_patient_id_seq OWNER TO gazelle;

--
-- Name: dds_patient_patient_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_patient_patient_address (
    patient_id integer NOT NULL,
    patient_address_id integer NOT NULL
);


ALTER TABLE public.dds_patient_patient_address OWNER TO gazelle;

--
-- Name: dds_person; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_person (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    date_of_birth timestamp without time zone,
    date_of_death timestamp without time zone,
    first_name_sex_id integer,
    last_name_id integer,
    race_id integer,
    religion_id integer,
    sex_id integer,
    mother_maiden_name_id integer,
    first_alternative_name character varying(255),
    last_alternative_name character varying(255),
    marital_status character varying(255),
    name_type character varying(255),
    last_modifier character varying(255)
);


ALTER TABLE public.dds_person OWNER TO gazelle;

--
-- Name: dds_person_dds_other_name; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_person_dds_other_name (
    dds_person_id integer NOT NULL,
    othernamelist_id integer NOT NULL
);


ALTER TABLE public.dds_person_dds_other_name OWNER TO gazelle;

--
-- Name: dds_person_first_name_sex; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_person_first_name_sex (
    person_id integer NOT NULL,
    first_name_sex_id integer NOT NULL
);


ALTER TABLE public.dds_person_first_name_sex OWNER TO gazelle;

--
-- Name: dds_person_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_person_id_seq OWNER TO gazelle;

--
-- Name: dds_race; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_race (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    code character varying(255),
    description character varying(255),
    last_modifier character varying(255)
);


ALTER TABLE public.dds_race OWNER TO gazelle;

--
-- Name: dds_race_by_country_code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_race_by_country_code (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    cumul integer,
    frequency integer,
    country_code_id integer,
    race_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_race_by_country_code OWNER TO gazelle;

--
-- Name: dds_race_by_country_code_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_race_by_country_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_race_by_country_code_id_seq OWNER TO gazelle;

--
-- Name: dds_race_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_race_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_race_id_seq OWNER TO gazelle;

--
-- Name: dds_religion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_religion (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    code character varying(255),
    description character varying(255),
    last_modifier character varying(255)
);


ALTER TABLE public.dds_religion OWNER TO gazelle;

--
-- Name: dds_religion_by_country_code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_religion_by_country_code (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    cumul integer,
    frequency integer,
    country_code_id integer,
    religion_id integer,
    last_modifier character varying(255)
);


ALTER TABLE public.dds_religion_by_country_code OWNER TO gazelle;

--
-- Name: dds_religion_by_country_code_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_religion_by_country_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_religion_by_country_code_id_seq OWNER TO gazelle;

--
-- Name: dds_religion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_religion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_religion_id_seq OWNER TO gazelle;

--
-- Name: dds_sex; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_sex (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    code character varying(255),
    description character varying(255),
    last_modifier character varying(255)
);


ALTER TABLE public.dds_sex OWNER TO gazelle;

--
-- Name: dds_sex_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_sex_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_sex_id_seq OWNER TO gazelle;

--
-- Name: dds_state; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_state (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    code character varying(255),
    name character varying(200),
    last_modifier character varying(255)
);


ALTER TABLE public.dds_state OWNER TO gazelle;

--
-- Name: dds_state_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_state_id_seq OWNER TO gazelle;

--
-- Name: dds_street; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_street (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    value character varying(255),
    last_modifier character varying(255)
);


ALTER TABLE public.dds_street OWNER TO gazelle;

--
-- Name: dds_street_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_street_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_street_id_seq OWNER TO gazelle;

--
-- Name: dds_user_ip_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_user_ip_address (
    id integer NOT NULL,
    geoname_request_number integer NOT NULL,
    google_request_number integer NOT NULL,
    ip_address character varying(255) NOT NULL,
    nominatim_request_number integer NOT NULL,
    request_time timestamp without time zone NOT NULL
);


ALTER TABLE public.dds_user_ip_address OWNER TO gazelle;

--
-- Name: dds_user_ip_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_user_ip_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_user_ip_address_id_seq OWNER TO gazelle;

--
-- Name: dds_user_request_historic; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_user_request_historic (
    id integer NOT NULL,
    failed_request_number integer NOT NULL,
    ip_address character varying(255) NOT NULL,
    last_request_time timestamp without time zone NOT NULL,
    request_number integer NOT NULL,
    unlimited boolean
);


ALTER TABLE public.dds_user_request_historic OWNER TO gazelle;

--
-- Name: dds_user_request_historic_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_user_request_historic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_user_request_historic_id_seq OWNER TO gazelle;

--
-- Name: dds_web_services_information; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.dds_web_services_information (
    id integer NOT NULL,
    last_user_ip_id integer NOT NULL,
    request_maximum_number integer NOT NULL,
    request_since_last_day integer NOT NULL,
    total_request integer NOT NULL,
    ws_name character varying(255) NOT NULL,
    ws_url character varying(255) NOT NULL,
    last_request_time timestamp without time zone,
    ws_priority integer
);


ALTER TABLE public.dds_web_services_information OWNER TO gazelle;

--
-- Name: dds_web_services_information_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.dds_web_services_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dds_web_services_information_id_seq OWNER TO gazelle;

--
-- Name: geoname; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.geoname (
    geonameid integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    admin1 character varying(20),
    admin2 character varying(80),
    admin3 character varying(20),
    admin4 character varying(20),
    alternatenames character varying(4000),
    asciiname character varying(200),
    cc2 character varying(60),
    country character varying(2),
    elevation integer,
    fclass character(1),
    fcode character varying(10),
    gtopo30 integer,
    latitude double precision,
    longitude double precision,
    moddate character varying(40),
    name character varying(200),
    population numeric(19,2),
    timezone character varying(40),
    last_modifier character varying(255)
);


ALTER TABLE public.geoname OWNER TO gazelle;

--
-- Name: geoname_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.geoname_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.geoname_id_seq OWNER TO gazelle;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO gazelle;

--
-- Name: revinfo; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.revinfo (
    rev integer NOT NULL,
    revtstmp bigint
);


ALTER TABLE public.revinfo OWNER TO gazelle;

--
-- Name: usr_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_address (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    address character varying(512),
    address_type character varying(255),
    city character varying(255),
    fax character varying(64),
    phone character varying(64),
    state character varying(255),
    zip_code character varying(20),
    country character varying(2)
);


ALTER TABLE public.usr_address OWNER TO gazelle;

--
-- Name: usr_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_address_id_seq OWNER TO gazelle;

--
-- Name: usr_currency; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_currency (
    keyword character varying(12) NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    comment character varying(255),
    name character varying(255)
);


ALTER TABLE public.usr_currency OWNER TO gazelle;

--
-- Name: usr_gazelle_language; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_gazelle_language (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(255) NOT NULL,
    keyword character varying(255) NOT NULL
);


ALTER TABLE public.usr_gazelle_language OWNER TO gazelle;

--
-- Name: usr_gazelle_language_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_gazelle_language_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_gazelle_language_id_seq OWNER TO gazelle;

--
-- Name: usr_institution; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_institution (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    activated boolean,
    integration_statements_repository_url character varying(512),
    keyword character varying(16) NOT NULL,
    name character varying(255) NOT NULL,
    note character varying(1024),
    url character varying(512) NOT NULL,
    institution_type_id integer NOT NULL,
    mailing_address_id integer
);


ALTER TABLE public.usr_institution OWNER TO gazelle;

--
-- Name: usr_institution_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_institution_address (
    institution_id integer NOT NULL,
    address_id integer NOT NULL
);


ALTER TABLE public.usr_institution_address OWNER TO gazelle;

--
-- Name: usr_institution_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_institution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_institution_id_seq OWNER TO gazelle;

--
-- Name: usr_institution_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_institution_type (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(255),
    type character varying(32) NOT NULL
);


ALTER TABLE public.usr_institution_type OWNER TO gazelle;

--
-- Name: usr_institution_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_institution_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_institution_type_id_seq OWNER TO gazelle;

--
-- Name: usr_iso_3166_country_code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_iso_3166_country_code (
    iso character varying(2) NOT NULL,
    ec boolean,
    flag_url character varying(255),
    iso3 character varying(3),
    name character varying(80) NOT NULL,
    numcode integer,
    printable_name character varying(80) NOT NULL
);


ALTER TABLE public.usr_iso_3166_country_code OWNER TO gazelle;

--
-- Name: usr_person; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_person (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    cell_phone character varying(64),
    email character varying(255),
    firstname character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL,
    mailing_list boolean,
    personal_fax character varying(64),
    personal_phone character varying(64),
    photo character varying(512),
    address_id integer,
    institution_id integer NOT NULL
);


ALTER TABLE public.usr_person OWNER TO gazelle;

--
-- Name: usr_person_function; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_person_function (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(1024),
    is_billing boolean,
    name character varying(255) NOT NULL,
    keyword character varying(255)
);


ALTER TABLE public.usr_person_function OWNER TO gazelle;

--
-- Name: usr_person_function_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_person_function_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_person_function_id_seq OWNER TO gazelle;

--
-- Name: usr_person_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_person_id_seq OWNER TO gazelle;

--
-- Name: usr_persons_functions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_persons_functions (
    person_id integer NOT NULL,
    person_function_id integer NOT NULL
);


ALTER TABLE public.usr_persons_functions OWNER TO gazelle;

--
-- Name: usr_role; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_role (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(1024),
    name character varying(64) NOT NULL
);


ALTER TABLE public.usr_role OWNER TO gazelle;

--
-- Name: usr_role_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_role_id_seq OWNER TO gazelle;

--
-- Name: usr_user_role; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_user_role (
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.usr_user_role OWNER TO gazelle;

--
-- Name: usr_users; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_users (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    activated boolean,
    activation_code character varying(255),
    blocked boolean,
    change_password_code character varying(255),
    creation_date timestamp without time zone,
    email character varying(255) NOT NULL,
    counter_failed_login_attempts integer,
    firstname character varying(128),
    last_login timestamp without time zone,
    lastname character varying(128),
    counter_logins integer,
    password character varying(128) NOT NULL,
    username character varying(16) NOT NULL,
    institution_id integer NOT NULL
);


ALTER TABLE public.usr_users OWNER TO gazelle;

--
-- Name: usr_users_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_users_id_seq OWNER TO gazelle;

--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: app_configuration app_configuration_variable_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT app_configuration_variable_key UNIQUE (variable);


--
-- Name: cmn_application_preference cmn_application_preference_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_application_preference
    ADD CONSTRAINT cmn_application_preference_pkey PRIMARY KEY (id);


--
-- Name: cmn_event_tracking cmn_event_tracking_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_event_tracking
    ADD CONSTRAINT cmn_event_tracking_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_iso3_language_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_iso3_language_key UNIQUE (iso3_language);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: cmn_number_of_results_per_page cmn_number_of_results_per_page_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_number_of_results_per_page
    ADD CONSTRAINT cmn_number_of_results_per_page_pkey PRIMARY KEY (id);


--
-- Name: cmn_path_linking_a_document cmn_path_linking_a_document_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_path_linking_a_document
    ADD CONSTRAINT cmn_path_linking_a_document_pkey PRIMARY KEY (id);


--
-- Name: dds_character_encoding dds_character_encoding_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_character_encoding
    ADD CONSTRAINT dds_character_encoding_pkey PRIMARY KEY (id);


--
-- Name: dds_character_set_encoding dds_character_set_encoding_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_character_set_encoding
    ADD CONSTRAINT dds_character_set_encoding_pkey PRIMARY KEY (id);


--
-- Name: dds_city_aud dds_city_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_city_aud
    ADD CONSTRAINT dds_city_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: dds_city dds_city_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_city
    ADD CONSTRAINT dds_city_pkey PRIMARY KEY (id);


--
-- Name: dds_country_code_character_set dds_country_code_character_set_character_set_encoding_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code_character_set
    ADD CONSTRAINT dds_country_code_character_set_character_set_encoding_id_key UNIQUE (character_set_encoding_id, country_code_id);


--
-- Name: dds_country_code dds_country_code_country_code_iso_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code
    ADD CONSTRAINT dds_country_code_country_code_iso_key UNIQUE (country_code_iso);


--
-- Name: dds_country_code dds_country_code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code
    ADD CONSTRAINT dds_country_code_pkey PRIMARY KEY (id);


--
-- Name: dds_first_name_alternate_spelling dds_first_name_alternate_spelling_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_alternate_spelling
    ADD CONSTRAINT dds_first_name_alternate_spelling_pkey PRIMARY KEY (id);


--
-- Name: dds_first_name_country_code dds_first_name_country_code_first_name_sex_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_country_code
    ADD CONSTRAINT dds_first_name_country_code_first_name_sex_id_key UNIQUE (first_name_sex_id, country_code_id);


--
-- Name: dds_first_name_country_code dds_first_name_country_code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_country_code
    ADD CONSTRAINT dds_first_name_country_code_pkey PRIMARY KEY (id);


--
-- Name: dds_first_name_sex dds_first_name_sex_gender_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_sex
    ADD CONSTRAINT dds_first_name_sex_gender_id_key UNIQUE (gender_id, value);


--
-- Name: dds_first_name_sex dds_first_name_sex_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_sex
    ADD CONSTRAINT dds_first_name_sex_pkey PRIMARY KEY (id);


--
-- Name: dds_gender dds_gender_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_gender
    ADD CONSTRAINT dds_gender_pkey PRIMARY KEY (id);


--
-- Name: dds_language dds_language_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_language
    ADD CONSTRAINT dds_language_pkey PRIMARY KEY (id);


--
-- Name: dds_last_name_alternate_spelling dds_last_name_alternate_spelling_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name_alternate_spelling
    ADD CONSTRAINT dds_last_name_alternate_spelling_pkey PRIMARY KEY (id);


--
-- Name: dds_last_name_country_code dds_last_name_country_code_country_code_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name_country_code
    ADD CONSTRAINT dds_last_name_country_code_country_code_id_key UNIQUE (country_code_id, last_name_id);


--
-- Name: dds_last_name_country_code dds_last_name_country_code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name_country_code
    ADD CONSTRAINT dds_last_name_country_code_pkey PRIMARY KEY (id);


--
-- Name: dds_last_name dds_last_name_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name
    ADD CONSTRAINT dds_last_name_pkey PRIMARY KEY (id);


--
-- Name: dds_last_name dds_last_name_value_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name
    ADD CONSTRAINT dds_last_name_value_key UNIQUE (value);


--
-- Name: dds_other_name dds_other_name_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_other_name
    ADD CONSTRAINT dds_other_name_pkey PRIMARY KEY (id);


--
-- Name: dds_patient_address dds_patient_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_address
    ADD CONSTRAINT dds_patient_address_pkey PRIMARY KEY (id);


--
-- Name: dds_patient_patient_address dds_patient_patient_address_patient_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_patient_address
    ADD CONSTRAINT dds_patient_patient_address_patient_id_key UNIQUE (patient_id, patient_address_id);


--
-- Name: dds_patient dds_patient_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient
    ADD CONSTRAINT dds_patient_pkey PRIMARY KEY (id);


--
-- Name: dds_person_dds_other_name dds_person_dds_other_name_othernamelist_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person_dds_other_name
    ADD CONSTRAINT dds_person_dds_other_name_othernamelist_id_key UNIQUE (othernamelist_id);


--
-- Name: dds_person dds_person_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT dds_person_pkey PRIMARY KEY (id);


--
-- Name: dds_race_by_country_code dds_race_by_country_code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_race_by_country_code
    ADD CONSTRAINT dds_race_by_country_code_pkey PRIMARY KEY (id);


--
-- Name: dds_race dds_race_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_race
    ADD CONSTRAINT dds_race_pkey PRIMARY KEY (id);


--
-- Name: dds_religion_by_country_code dds_religion_by_country_code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_religion_by_country_code
    ADD CONSTRAINT dds_religion_by_country_code_pkey PRIMARY KEY (id);


--
-- Name: dds_religion dds_religion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_religion
    ADD CONSTRAINT dds_religion_pkey PRIMARY KEY (id);


--
-- Name: dds_sex dds_sex_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_sex
    ADD CONSTRAINT dds_sex_pkey PRIMARY KEY (id);


--
-- Name: dds_state dds_state_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_state
    ADD CONSTRAINT dds_state_pkey PRIMARY KEY (id);


--
-- Name: dds_street dds_street_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_street
    ADD CONSTRAINT dds_street_pkey PRIMARY KEY (id);


--
-- Name: dds_user_ip_address dds_user_ip_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_user_ip_address
    ADD CONSTRAINT dds_user_ip_address_pkey PRIMARY KEY (id);


--
-- Name: dds_user_request_historic dds_user_request_historic_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_user_request_historic
    ADD CONSTRAINT dds_user_request_historic_pkey PRIMARY KEY (id);


--
-- Name: dds_web_services_information dds_web_services_information_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_web_services_information
    ADD CONSTRAINT dds_web_services_information_pkey PRIMARY KEY (id);


--
-- Name: geoname geoname_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.geoname
    ADD CONSTRAINT geoname_pkey PRIMARY KEY (geonameid);


--
-- Name: revinfo revinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.revinfo
    ADD CONSTRAINT revinfo_pkey PRIMARY KEY (rev);


--
-- Name: app_configuration uk_20rnkdjn5jvlvmsb5f7io4b1o; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT uk_20rnkdjn5jvlvmsb5f7io4b1o UNIQUE (variable);


--
-- Name: dds_last_name uk_2d3280cpyiuc9by7qr1aj2whw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name
    ADD CONSTRAINT uk_2d3280cpyiuc9by7qr1aj2whw UNIQUE (gender_id, value);


--
-- Name: dds_person_dds_other_name uk_55a0r8bj9mlwvqrld64902hst; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person_dds_other_name
    ADD CONSTRAINT uk_55a0r8bj9mlwvqrld64902hst UNIQUE (othernamelist_id);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: dds_patient_patient_address uk_p0rg5fitn4vmgcqk2a7785vnv; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_patient_address
    ADD CONSTRAINT uk_p0rg5fitn4vmgcqk2a7785vnv UNIQUE (patient_id, patient_address_id);


--
-- Name: dds_first_name_sex uk_s7xu6nvlyr31bvsaft1qkm733; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_sex
    ADD CONSTRAINT uk_s7xu6nvlyr31bvsaft1qkm733 UNIQUE (gender_id, value);


--
-- Name: dds_country_code_character_set uk_vn33xerqy9br7c3x1uwtjnhc; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code_character_set
    ADD CONSTRAINT uk_vn33xerqy9br7c3x1uwtjnhc UNIQUE (character_set_encoding_id, country_code_id);


--
-- Name: usr_address usr_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_address
    ADD CONSTRAINT usr_address_pkey PRIMARY KEY (id);


--
-- Name: usr_currency usr_currency_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_currency
    ADD CONSTRAINT usr_currency_pkey PRIMARY KEY (keyword);


--
-- Name: usr_gazelle_language usr_gazelle_language_description_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_gazelle_language
    ADD CONSTRAINT usr_gazelle_language_description_key UNIQUE (description);


--
-- Name: usr_gazelle_language usr_gazelle_language_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_gazelle_language
    ADD CONSTRAINT usr_gazelle_language_keyword_key UNIQUE (keyword);


--
-- Name: usr_gazelle_language usr_gazelle_language_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_gazelle_language
    ADD CONSTRAINT usr_gazelle_language_pkey PRIMARY KEY (id);


--
-- Name: usr_institution_address usr_institution_address_institution_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution_address
    ADD CONSTRAINT usr_institution_address_institution_id_key UNIQUE (institution_id, address_id);


--
-- Name: usr_institution usr_institution_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution
    ADD CONSTRAINT usr_institution_name_key UNIQUE (name);


--
-- Name: usr_institution usr_institution_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution
    ADD CONSTRAINT usr_institution_pkey PRIMARY KEY (id);


--
-- Name: usr_institution_type usr_institution_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution_type
    ADD CONSTRAINT usr_institution_type_pkey PRIMARY KEY (id);


--
-- Name: usr_institution_type usr_institution_type_type_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution_type
    ADD CONSTRAINT usr_institution_type_type_key UNIQUE (type);


--
-- Name: usr_iso_3166_country_code usr_iso_3166_country_code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_iso_3166_country_code
    ADD CONSTRAINT usr_iso_3166_country_code_pkey PRIMARY KEY (iso);


--
-- Name: usr_person_function usr_person_function_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_person_function
    ADD CONSTRAINT usr_person_function_keyword_key UNIQUE (keyword);


--
-- Name: usr_person_function usr_person_function_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_person_function
    ADD CONSTRAINT usr_person_function_pkey PRIMARY KEY (id);


--
-- Name: usr_person usr_person_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_person
    ADD CONSTRAINT usr_person_pkey PRIMARY KEY (id);


--
-- Name: usr_persons_functions usr_persons_functions_person_function_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_persons_functions
    ADD CONSTRAINT usr_persons_functions_person_function_id_key UNIQUE (person_function_id, person_id);


--
-- Name: usr_role usr_role_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_role
    ADD CONSTRAINT usr_role_pkey PRIMARY KEY (id);


--
-- Name: usr_user_role usr_user_role_user_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_user_role
    ADD CONSTRAINT usr_user_role_user_id_key UNIQUE (user_id, role_id);


--
-- Name: usr_users usr_users_email_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_users
    ADD CONSTRAINT usr_users_email_key UNIQUE (email);


--
-- Name: usr_users usr_users_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_users
    ADD CONSTRAINT usr_users_pkey PRIMARY KEY (id);


--
-- Name: usr_users usr_users_username_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_users
    ADD CONSTRAINT usr_users_username_key UNIQUE (username);


--
-- Name: dds_other_name fk109d8a2655c92c3c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_other_name
    ADD CONSTRAINT fk109d8a2655c92c3c FOREIGN KEY (last_name_id) REFERENCES public.dds_last_name(id);


--
-- Name: dds_other_name fk109d8a265f52392e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_other_name
    ADD CONSTRAINT fk109d8a265f52392e FOREIGN KEY (last_name_id) REFERENCES public.dds_last_name(id);


--
-- Name: dds_other_name fk109d8a266ee1428b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_other_name
    ADD CONSTRAINT fk109d8a266ee1428b FOREIGN KEY (first_name_sex_id) REFERENCES public.dds_first_name_sex(id);


--
-- Name: dds_other_name fk109d8a269b6bfe99; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_other_name
    ADD CONSTRAINT fk109d8a269b6bfe99 FOREIGN KEY (first_name_sex_id) REFERENCES public.dds_first_name_sex(id);


--
-- Name: usr_address fk18243449e334b3e8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_address
    ADD CONSTRAINT fk18243449e334b3e8 FOREIGN KEY (country) REFERENCES public.usr_iso_3166_country_code(iso);


--
-- Name: dds_country_code_language fk236c993543f7a600; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code_language
    ADD CONSTRAINT fk236c993543f7a600 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_country_code_language fk236c99357db94e0f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code_language
    ADD CONSTRAINT fk236c99357db94e0f FOREIGN KEY (language_id) REFERENCES public.dds_language(id);


--
-- Name: dds_country_code_language fk236c993587425b01; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code_language
    ADD CONSTRAINT fk236c993587425b01 FOREIGN KEY (language_id) REFERENCES public.dds_language(id);


--
-- Name: dds_country_code_language fk236c9935a03e3032; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code_language
    ADD CONSTRAINT fk236c9935a03e3032 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_last_name_alternate_spelling fk284b83655c92c3c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name_alternate_spelling
    ADD CONSTRAINT fk284b83655c92c3c FOREIGN KEY (last_name_id) REFERENCES public.dds_last_name(id);


--
-- Name: dds_last_name_alternate_spelling fk284b8365f52392e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name_alternate_spelling
    ADD CONSTRAINT fk284b8365f52392e FOREIGN KEY (last_name_id) REFERENCES public.dds_last_name(id);


--
-- Name: dds_race_by_country_code fk29bc3b3c24c5e1ef; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_race_by_country_code
    ADD CONSTRAINT fk29bc3b3c24c5e1ef FOREIGN KEY (race_id) REFERENCES public.dds_race(id);


--
-- Name: dds_race_by_country_code fk29bc3b3c43f7a600; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_race_by_country_code
    ADD CONSTRAINT fk29bc3b3c43f7a600 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_race_by_country_code fk29bc3b3ca03e3032; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_race_by_country_code
    ADD CONSTRAINT fk29bc3b3ca03e3032 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_race_by_country_code fk29bc3b3cfaf37e1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_race_by_country_code
    ADD CONSTRAINT fk29bc3b3cfaf37e1 FOREIGN KEY (race_id) REFERENCES public.dds_race(id);


--
-- Name: dds_last_name_country_code fk2b5de28d43f7a600; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name_country_code
    ADD CONSTRAINT fk2b5de28d43f7a600 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_last_name_country_code fk2b5de28d55c92c3c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name_country_code
    ADD CONSTRAINT fk2b5de28d55c92c3c FOREIGN KEY (last_name_id) REFERENCES public.dds_last_name(id);


--
-- Name: dds_last_name_country_code fk2b5de28d5f52392e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name_country_code
    ADD CONSTRAINT fk2b5de28d5f52392e FOREIGN KEY (last_name_id) REFERENCES public.dds_last_name(id);


--
-- Name: dds_last_name_country_code fk2b5de28da03e3032; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name_country_code
    ADD CONSTRAINT fk2b5de28da03e3032 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_last_name fk2e8e394853551c21; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name
    ADD CONSTRAINT fk2e8e394853551c21 FOREIGN KEY (gender_id) REFERENCES public.dds_gender(id);


--
-- Name: dds_last_name fk2e8e39487d697aaf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_last_name
    ADD CONSTRAINT fk2e8e39487d697aaf FOREIGN KEY (gender_id) REFERENCES public.dds_gender(id);


--
-- Name: dds_first_name_sex fk3ea4b32d53551c21; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_sex
    ADD CONSTRAINT fk3ea4b32d53551c21 FOREIGN KEY (gender_id) REFERENCES public.dds_gender(id);


--
-- Name: dds_first_name_sex fk3ea4b32d7d697aaf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_sex
    ADD CONSTRAINT fk3ea4b32d7d697aaf FOREIGN KEY (gender_id) REFERENCES public.dds_gender(id);


--
-- Name: dds_country_code_character_set fk4355068f43f7a600; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code_character_set
    ADD CONSTRAINT fk4355068f43f7a600 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_country_code_character_set fk4355068f8fe600c7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code_character_set
    ADD CONSTRAINT fk4355068f8fe600c7 FOREIGN KEY (character_set_encoding_id) REFERENCES public.dds_character_set_encoding(id);


--
-- Name: dds_country_code_character_set fk4355068fa03e3032; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code_character_set
    ADD CONSTRAINT fk4355068fa03e3032 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_country_code_character_set fk4355068fefb2ced5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code_character_set
    ADD CONSTRAINT fk4355068fefb2ced5 FOREIGN KEY (character_set_encoding_id) REFERENCES public.dds_character_set_encoding(id);


--
-- Name: dds_patient_patient_address fk521c8f34424a9784; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_patient_address
    ADD CONSTRAINT fk521c8f34424a9784 FOREIGN KEY (patient_address_id) REFERENCES public.dds_patient_address(id);


--
-- Name: dds_patient_patient_address fk521c8f34aa046a73; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_patient_address
    ADD CONSTRAINT fk521c8f34aa046a73 FOREIGN KEY (patient_id) REFERENCES public.dds_patient(id);


--
-- Name: dds_patient_patient_address fk521c8f34c27bdda5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_patient_address
    ADD CONSTRAINT fk521c8f34c27bdda5 FOREIGN KEY (patient_id) REFERENCES public.dds_patient(id);


--
-- Name: dds_patient_patient_address fk521c8f34d7ea6f6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_patient_address
    ADD CONSTRAINT fk521c8f34d7ea6f6 FOREIGN KEY (patient_address_id) REFERENCES public.dds_patient_address(id);


--
-- Name: usr_persons_functions fk5593ae6f7856fd97; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_persons_functions
    ADD CONSTRAINT fk5593ae6f7856fd97 FOREIGN KEY (person_function_id) REFERENCES public.usr_person_function(id);


--
-- Name: usr_persons_functions fk5593ae6f99a265ec; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_persons_functions
    ADD CONSTRAINT fk5593ae6f99a265ec FOREIGN KEY (person_id) REFERENCES public.usr_person(id);


--
-- Name: dds_patient_address fk5e30ec6e19e123af; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_address
    ADD CONSTRAINT fk5e30ec6e19e123af FOREIGN KEY (city_id) REFERENCES public.dds_city(id);


--
-- Name: dds_patient_address fk5e30ec6e38b25133; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_address
    ADD CONSTRAINT fk5e30ec6e38b25133 FOREIGN KEY (state_id) REFERENCES public.dds_state(id);


--
-- Name: dds_patient_address fk5e30ec6e4759fce1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_address
    ADD CONSTRAINT fk5e30ec6e4759fce1 FOREIGN KEY (street_id) REFERENCES public.dds_street(id);


--
-- Name: dds_patient_address fk5e30ec6e4afcb192; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_address
    ADD CONSTRAINT fk5e30ec6e4afcb192 FOREIGN KEY (country_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_patient_address fk5e30ec6e4ca79a1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_address
    ADD CONSTRAINT fk5e30ec6e4ca79a1 FOREIGN KEY (city_id) REFERENCES public.dds_city(id);


--
-- Name: dds_patient_address fk5e30ec6e716e5b6f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_address
    ADD CONSTRAINT fk5e30ec6e716e5b6f FOREIGN KEY (street_id) REFERENCES public.dds_street(id);


--
-- Name: dds_patient_address fk5e30ec6ec670e8e5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_address
    ADD CONSTRAINT fk5e30ec6ec670e8e5 FOREIGN KEY (state_id) REFERENCES public.dds_state(id);


--
-- Name: dds_patient_address fk5e30ec6eeeb62760; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient_address
    ADD CONSTRAINT fk5e30ec6eeeb62760 FOREIGN KEY (country_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_person fk67e64d2124c5e1ef; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d2124c5e1ef FOREIGN KEY (race_id) REFERENCES public.dds_race(id);


--
-- Name: dds_person fk67e64d212783753; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d212783753 FOREIGN KEY (sex_id) REFERENCES public.dds_sex(id);


--
-- Name: dds_person fk67e64d212a024f3a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d212a024f3a FOREIGN KEY (mother_maiden_name_id) REFERENCES public.dds_last_name(id);


--
-- Name: dds_person fk67e64d21338b5c2c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d21338b5c2c FOREIGN KEY (mother_maiden_name_id) REFERENCES public.dds_last_name(id);


--
-- Name: dds_person fk67e64d2155c92c3c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d2155c92c3c FOREIGN KEY (last_name_id) REFERENCES public.dds_last_name(id);


--
-- Name: dds_person fk67e64d215dfd1385; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d215dfd1385 FOREIGN KEY (sex_id) REFERENCES public.dds_sex(id);


--
-- Name: dds_person fk67e64d215f52392e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d215f52392e FOREIGN KEY (last_name_id) REFERENCES public.dds_last_name(id);


--
-- Name: dds_person fk67e64d216ee1428b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d216ee1428b FOREIGN KEY (first_name_sex_id) REFERENCES public.dds_first_name_sex(id);


--
-- Name: dds_person fk67e64d219b6bfe99; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d219b6bfe99 FOREIGN KEY (first_name_sex_id) REFERENCES public.dds_first_name_sex(id);


--
-- Name: dds_person fk67e64d21dc85536f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d21dc85536f FOREIGN KEY (religion_id) REFERENCES public.dds_religion(id);


--
-- Name: dds_person fk67e64d21e60e6061; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d21e60e6061 FOREIGN KEY (religion_id) REFERENCES public.dds_religion(id);


--
-- Name: dds_person fk67e64d21faf37e1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person
    ADD CONSTRAINT fk67e64d21faf37e1 FOREIGN KEY (race_id) REFERENCES public.dds_race(id);


--
-- Name: dds_person_dds_other_name fk6beda5043d67ce55; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person_dds_other_name
    ADD CONSTRAINT fk6beda5043d67ce55 FOREIGN KEY (dds_person_id) REFERENCES public.dds_person(id);


--
-- Name: dds_person_dds_other_name fk6beda504677c2ce3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person_dds_other_name
    ADD CONSTRAINT fk6beda504677c2ce3 FOREIGN KEY (dds_person_id) REFERENCES public.dds_person(id);


--
-- Name: dds_person_dds_other_name fk6beda5046819c367; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person_dds_other_name
    ADD CONSTRAINT fk6beda5046819c367 FOREIGN KEY (othernamelist_id) REFERENCES public.dds_other_name(id);


--
-- Name: dds_person_dds_other_name fk6beda5048fb254b5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person_dds_other_name
    ADD CONSTRAINT fk6beda5048fb254b5 FOREIGN KEY (othernamelist_id) REFERENCES public.dds_other_name(id);


--
-- Name: dds_first_name_alternate_spelling fk740ec2386ee1428b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_alternate_spelling
    ADD CONSTRAINT fk740ec2386ee1428b FOREIGN KEY (first_name_sex_id) REFERENCES public.dds_first_name_sex(id);


--
-- Name: dds_first_name_alternate_spelling fk740ec2389b6bfe99; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_alternate_spelling
    ADD CONSTRAINT fk740ec2389b6bfe99 FOREIGN KEY (first_name_sex_id) REFERENCES public.dds_first_name_sex(id);


--
-- Name: usr_user_role fk8003cb7f96e9f6ac; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_user_role
    ADD CONSTRAINT fk8003cb7f96e9f6ac FOREIGN KEY (user_id) REFERENCES public.usr_users(id);


--
-- Name: usr_user_role fk8003cb7ff1bf32cc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_user_role
    ADD CONSTRAINT fk8003cb7ff1bf32cc FOREIGN KEY (role_id) REFERENCES public.usr_role(id);


--
-- Name: dds_country_code fk883885c242c97fa1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code
    ADD CONSTRAINT fk883885c242c97fa1 FOREIGN KEY (character_set_encoding_id_reference) REFERENCES public.dds_character_set_encoding(id);


--
-- Name: dds_country_code fk883885c2e2fcb193; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_country_code
    ADD CONSTRAINT fk883885c2e2fcb193 FOREIGN KEY (character_set_encoding_id_reference) REFERENCES public.dds_character_set_encoding(id);


--
-- Name: dds_language fk8cf00224b9a2ea6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_language
    ADD CONSTRAINT fk8cf00224b9a2ea6 FOREIGN KEY (character_encoding_id) REFERENCES public.dds_character_encoding(id);


--
-- Name: dds_language fk8cf00224c48cb58; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_language
    ADD CONSTRAINT fk8cf00224c48cb58 FOREIGN KEY (character_encoding_id) REFERENCES public.dds_character_encoding(id);


--
-- Name: dds_patient fk8e2772b9298894fb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient
    ADD CONSTRAINT fk8e2772b9298894fb FOREIGN KEY (person_dds_id) REFERENCES public.dds_person(id);


--
-- Name: dds_patient fk8e2772b93feacd76; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient
    ADD CONSTRAINT fk8e2772b93feacd76 FOREIGN KEY (motherpatient_id) REFERENCES public.dds_patient(id);


--
-- Name: dds_patient fk8e2772b94afcb192; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient
    ADD CONSTRAINT fk8e2772b94afcb192 FOREIGN KEY (country_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_patient fk8e2772b9586240a8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient
    ADD CONSTRAINT fk8e2772b9586240a8 FOREIGN KEY (motherpatient_id) REFERENCES public.dds_patient(id);


--
-- Name: dds_patient fk8e2772b9eeb62760; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient
    ADD CONSTRAINT fk8e2772b9eeb62760 FOREIGN KEY (country_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_patient fk8e2772b9ff74366d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_patient
    ADD CONSTRAINT fk8e2772b9ff74366d FOREIGN KEY (person_dds_id) REFERENCES public.dds_person(id);


--
-- Name: dds_city_aud fk98678888df74e053; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_city_aud
    ADD CONSTRAINT fk98678888df74e053 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: dds_person_first_name_sex fka1aaa83f6ee1428b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person_first_name_sex
    ADD CONSTRAINT fka1aaa83f6ee1428b FOREIGN KEY (first_name_sex_id) REFERENCES public.dds_first_name_sex(id);


--
-- Name: dds_person_first_name_sex fka1aaa83f86d763a1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person_first_name_sex
    ADD CONSTRAINT fka1aaa83f86d763a1 FOREIGN KEY (person_id) REFERENCES public.dds_person(id);


--
-- Name: dds_person_first_name_sex fka1aaa83f9b6bfe99; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person_first_name_sex
    ADD CONSTRAINT fka1aaa83f9b6bfe99 FOREIGN KEY (first_name_sex_id) REFERENCES public.dds_first_name_sex(id);


--
-- Name: dds_person_first_name_sex fka1aaa83fb0ebc22f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_person_first_name_sex
    ADD CONSTRAINT fka1aaa83fb0ebc22f FOREIGN KEY (person_id) REFERENCES public.dds_person(id);


--
-- Name: usr_users fka4b7379d2fde29a8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_users
    ADD CONSTRAINT fka4b7379d2fde29a8 FOREIGN KEY (institution_id) REFERENCES public.usr_institution(id);


--
-- Name: usr_institution fkce580b8dcfa3ed1c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution
    ADD CONSTRAINT fkce580b8dcfa3ed1c FOREIGN KEY (mailing_address_id) REFERENCES public.usr_address(id);


--
-- Name: usr_institution fkce580b8dde0b0db9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution
    ADD CONSTRAINT fkce580b8dde0b0db9 FOREIGN KEY (institution_type_id) REFERENCES public.usr_institution_type(id);


--
-- Name: usr_institution_address fkd8f571422fde29a8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution_address
    ADD CONSTRAINT fkd8f571422fde29a8 FOREIGN KEY (institution_id) REFERENCES public.usr_institution(id);


--
-- Name: usr_institution_address fkd8f57142a6285728; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution_address
    ADD CONSTRAINT fkd8f57142a6285728 FOREIGN KEY (address_id) REFERENCES public.usr_address(id);


--
-- Name: dds_first_name_country_code fke7b0160f43f7a600; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_country_code
    ADD CONSTRAINT fke7b0160f43f7a600 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_first_name_country_code fke7b0160f6ee1428b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_country_code
    ADD CONSTRAINT fke7b0160f6ee1428b FOREIGN KEY (first_name_sex_id) REFERENCES public.dds_first_name_sex(id);


--
-- Name: dds_first_name_country_code fke7b0160f9b6bfe99; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_country_code
    ADD CONSTRAINT fke7b0160f9b6bfe99 FOREIGN KEY (first_name_sex_id) REFERENCES public.dds_first_name_sex(id);


--
-- Name: dds_first_name_country_code fke7b0160fa03e3032; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_first_name_country_code
    ADD CONSTRAINT fke7b0160fa03e3032 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: usr_person fke8e824602fde29a8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_person
    ADD CONSTRAINT fke8e824602fde29a8 FOREIGN KEY (institution_id) REFERENCES public.usr_institution(id);


--
-- Name: usr_person fke8e82460a6285728; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_person
    ADD CONSTRAINT fke8e82460a6285728 FOREIGN KEY (address_id) REFERENCES public.usr_address(id);


--
-- Name: dds_religion_by_country_code fkea61b79c43f7a600; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_religion_by_country_code
    ADD CONSTRAINT fkea61b79c43f7a600 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_religion_by_country_code fkea61b79ca03e3032; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_religion_by_country_code
    ADD CONSTRAINT fkea61b79ca03e3032 FOREIGN KEY (country_code_id) REFERENCES public.dds_country_code(id);


--
-- Name: dds_religion_by_country_code fkea61b79cdc85536f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_religion_by_country_code
    ADD CONSTRAINT fkea61b79cdc85536f FOREIGN KEY (religion_id) REFERENCES public.dds_religion(id);


--
-- Name: dds_religion_by_country_code fkea61b79ce60e6061; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.dds_religion_by_country_code
    ADD CONSTRAINT fkea61b79ce60e6061 FOREIGN KEY (religion_id) REFERENCES public.dds_religion(id);


--
-- PostgreSQL database dump complete
--

