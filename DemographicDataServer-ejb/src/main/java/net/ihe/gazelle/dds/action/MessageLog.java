package net.ihe.gazelle.dds.action;


public class MessageLog {


    private String ack;
    private String ackCode;
    private String sentMessage;


    public String getAck() {
        return ack;
    }

    public void setAck(String ack) {
        this.ack = ack;
    }

    public String getAckCode() {
        return ackCode;
    }

    public void setAckCode(String ackCode) {
        this.ackCode = ackCode;
    }

    public String getSentMessage() {
        return sentMessage;
    }

    public void setSentMessage(String sentMessage) {
        this.sentMessage = sentMessage;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ack == null) ? 0 : ack.hashCode());
        result = prime * result + ((ackCode == null) ? 0 : ackCode.hashCode());
        result = prime * result
                + ((sentMessage == null) ? 0 : sentMessage.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MessageLog other = (MessageLog) obj;
        if (ack == null) {
            if (other.ack != null) {
                return false;
            }
        } else if (!ack.equals(other.ack)) {
            return false;
        }
        if (ackCode == null) {
            if (other.ackCode != null) {
                return false;
            }
        } else if (!ackCode.equals(other.ackCode)) {
            return false;
        }
        if (sentMessage == null) {
            if (other.sentMessage != null) {
                return false;
            }
        } else if (!sentMessage.equals(other.sentMessage)) {
            return false;
        }
        return true;
    }


    public MessageLog(String ack, String ackCode, String sentMessage) {
        super();
        this.ack = ack;
        this.ackCode = ackCode;
        this.sentMessage = sentMessage;
    }


    public MessageLog() {
        super();
        // TODO Auto-generated constructor stub
    }


}
