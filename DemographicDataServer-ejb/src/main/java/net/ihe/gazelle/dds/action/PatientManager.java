/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.action;


import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.dds.model.Patient;
import net.ihe.gazelle.dds.model.PatientQuery;
import net.ihe.gazelle.dds.utils.Utils;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author abderrazek boufahja
 */
@Name("patientManager")
@Scope(ScopeType.PAGE)
public class PatientManager implements Serializable, QueryModifier<Patient> {

    private static final String PTS = "patientsToShare";
    /**
     *
     */
    private static final long serialVersionUID = -8734734233751607120L;
    private FilterDataModel<Patient> patientDataModel;
    private Filter<Patient> filter;
    private String firstname;
    private String lastname;
    private String firstnameAlter;
    private String lastnameAlter;
    private List<Patient> selectedPatientList = new ArrayList<Patient>();
    private Patient selectedPatientToView;

    public Patient getSelectedPatientToView() {
        return selectedPatientToView;
    }

    public void setSelectedPatientToView(Patient selectedPatientToView) {
        this.selectedPatientToView = selectedPatientToView;
    }

    public List<Patient> getSelectedPatientList() {
        return selectedPatientList;
    }

    public void setSelectedPatientList(List<Patient> selectedPatientList) {
        this.selectedPatientList = selectedPatientList;
    }

    @Create
    public void init() {
        selectedPatientList = new ArrayList<Patient>();
        Contexts.getSessionContext().set(PTS, null);
    }

    public String shareSelectedPatientList() {
        Contexts.getSessionContext().set(PTS, selectedPatientList);
        return "/patient/sharePatient.xhtml";
    }

    public String getMaritalStatusDescription(String value) {
        return Utils.getMaritalStatusDescription(value);
    }

    public String getNameTypeDescription(String value) {
        return Utils.getNameTypeDescription(value);
    }

    public void addPatientToSharePatientList(Patient patient) {
        List<Integer> patientIdList = new ArrayList<Integer>();
        for (Patient patientOfList : selectedPatientList) {
            patientIdList.add(patientOfList.getId());
        }

        if (!patientIdList.contains(patient.getId())) {
            selectedPatientList.add(patient);
        }
    }

    public void resetSelectedPatientList() {
        selectedPatientList = new ArrayList<Patient>();
        Contexts.getSessionContext().set(PTS, null);
    }

    public void removeSelectedPatient(Patient patient) {
        selectedPatientList.remove(patient);
        Contexts.getSessionContext().set(PTS, selectedPatientList);
    }

    public FilterDataModel<Patient> getPatientDataModel() {
        if (patientDataModel == null) {
            patientDataModel = new FilterDataModel<Patient>(getFilter()) {
                @Override
                protected Object getId(Patient t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return patientDataModel;
    }

    public void resetFilter() {
        getFilter().clear();
        lastname = null;
        firstname = null;
        lastnameAlter = null;
        firstnameAlter = null;
        getPatientDataModel().resetCache();
    }

    public Filter<Patient> getFilter() {
        if (filter == null) {
            filter = new Filter<Patient>(getHQLCriterionsForPatientFilter(),
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        }
        return filter;
    }

    private HQLCriterionsForFilter<Patient> getHQLCriterionsForPatientFilter() {
        PatientQuery query = new PatientQuery();
        HQLCriterionsForFilter<Patient> criterions = query.getHQLCriterionsForFilter();
        criterions.addPath("date", query.creationDate());
        criterions.addPath("country", query.country());
        criterions.addPath("gender", query.person().sex());
        criterions.addPath("religion", query.person().religion());
        criterions.addPath("race", query.person().race());
        criterions.addQueryModifier(this);
        return criterions;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<Patient> queryBuilder, Map<String, Object> filterValuesApplied) {
        if (firstname != null && !firstname.isEmpty()) {
            queryBuilder.addLike("person.firstNameSex.firstName", firstname);
        }
        if (lastname != null && !lastname.isEmpty()) {
            queryBuilder.addLike("person.lastName.value", lastname);
        }
        if (firstnameAlter != null && !firstnameAlter.isEmpty()) {
            queryBuilder.addLike("person.firstAlternativeName", firstnameAlter);
        }
        if (lastnameAlter != null && !lastnameAlter.isEmpty()) {
            queryBuilder.addLike("person.lastAlternativeName", lastnameAlter);
        }
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
        getFilter().modified();
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
        getFilter().modified();
    }

    public String getLastnameAlter() {
        return lastnameAlter;
    }

    public void setLastnameAlter(String lastnameAlter) {
        this.lastnameAlter = lastnameAlter;
    }

    public String getFirstnameAlter() {
        return firstnameAlter;
    }

    public void setFirstnameAlter(String firstnameAlter) {
        this.firstnameAlter = firstnameAlter;
    }

    public String getPermanentLink(Patient inPatient) {
        return "/patient/patient.seam?id=" + inPatient.getId();
    }
}
