package net.ihe.gazelle.dds.admin;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.dds.model.*;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.international.LocaleSelector;
import org.jboss.seam.log.Log;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import java.io.Serializable;
import java.util.*;


/**
 * @author Nicolas Lefebvre
 */


@Stateful
@Synchronized(timeout = 100000)
@Name("statisticManager")
@Scope(ScopeType.SESSION)
@GenerateInterface("StatisticManagerLocal")
public class StatisticManager implements StatisticManagerLocal, Serializable {


    private static final long serialVersionUID = -8734734853751607120L;

    /**
     * Logger
     */
    @Logger
    private static Log log;

    private String selectedTab = StatisticsTabManager.USER_HISTORIC_TAB;

    private int requestOfToday;
    private int requestOfYesterday;
    private int requestOfThisWeek;
    private int requestOfPreviousWeek;
    private int requestOfThisMonth;
    private int requestOfPreviousMonth;
    private int requestOfThisYear;
    private int requestOfPreviousYear;
    private int requestSinceTheBegining;
    private Filter<UserRequestHistoric> filterUserRequestHistoric;
    private Filter<UserIpAddress> filterUserIpAddress;
    private Filter<WebServicesInformation> filterWebServicesInformation;

    private Integer getRequestCount(Date startDate, Date endDate) {
        UserIpAddressQuery query = new UserIpAddressQuery();
        if (startDate != null) {
            query.requestTime().ge(startDate);
        }
        if (endDate != null) {
            query.requestTime().le(endDate);
        }
        return query.getCount();
    }

    public void init() {
        Locale locale = LocaleSelector.instance().getLocale();
        int firstDayOfWeekValue = Calendar.getInstance(locale).getFirstDayOfWeek();

        Calendar today = Calendar.getInstance(locale);
        Calendar yesterday = Calendar.getInstance(locale);
        Calendar firstDayOfWeek = Calendar.getInstance(locale);
        Calendar firstDayOfPreviousWeek = Calendar.getInstance(locale);
        Calendar firstDayOfMonth = Calendar.getInstance(locale);
        Calendar firstDayOfPreviousMonth = Calendar.getInstance(locale);
        Calendar firstDayOfYear = Calendar.getInstance(locale);
        Calendar firstDayOfPreviousYear = Calendar.getInstance(locale);

        today.setFirstDayOfWeek(firstDayOfWeekValue);
        today.setFirstDayOfWeek(firstDayOfWeekValue);
        today.set(firstDayOfWeek.get(Calendar.YEAR),
                firstDayOfWeek.get(Calendar.MONTH),
                firstDayOfWeek.get(Calendar.DATE), 0, 0, 0);

        yesterday.setFirstDayOfWeek(firstDayOfWeekValue);
        yesterday.set(firstDayOfWeek.get(Calendar.YEAR),
                firstDayOfWeek.get(Calendar.MONTH),
                firstDayOfWeek.get(Calendar.DATE), 0, 0, 0);
        yesterday.add(Calendar.DAY_OF_YEAR, -1);

        firstDayOfWeek.setFirstDayOfWeek(firstDayOfWeekValue);
        firstDayOfWeek.set(firstDayOfWeek.get(Calendar.YEAR),
                firstDayOfWeek.get(Calendar.MONTH),
                firstDayOfWeek.get(Calendar.DATE), 0, 0, 0);

        firstDayOfPreviousWeek.setFirstDayOfWeek(firstDayOfWeekValue);
        firstDayOfPreviousWeek.set(firstDayOfPreviousWeek.get(Calendar.YEAR),
                firstDayOfPreviousWeek.get(Calendar.MONTH),
                firstDayOfPreviousWeek.get(Calendar.DATE), 0, 0, 0);

        firstDayOfMonth.setFirstDayOfWeek(firstDayOfWeekValue);
        firstDayOfMonth.set(firstDayOfWeek.get(Calendar.YEAR),
                firstDayOfMonth.get(Calendar.MONTH),
                firstDayOfMonth.get(Calendar.DATE), 0, 0, 0);

        firstDayOfPreviousMonth.setFirstDayOfWeek(firstDayOfWeekValue);
        firstDayOfPreviousMonth.set(firstDayOfWeek.get(Calendar.YEAR),
                firstDayOfPreviousMonth.get(Calendar.MONTH),
                firstDayOfPreviousMonth.get(Calendar.DATE), 0, 0, 0);

        firstDayOfYear.setFirstDayOfWeek(firstDayOfWeekValue);
        firstDayOfYear.set(firstDayOfWeek.get(Calendar.YEAR),
                firstDayOfYear.get(Calendar.MONTH),
                firstDayOfYear.get(Calendar.DATE), 0, 0, 0);

        firstDayOfPreviousYear.setFirstDayOfWeek(firstDayOfWeekValue);
        firstDayOfPreviousYear.set(firstDayOfWeek.get(Calendar.YEAR) - 1,
                firstDayOfYear.get(Calendar.MONTH),
                firstDayOfYear.get(Calendar.DATE), 0, 0, 0);


        final int currentDayOfWeek = (today.get(Calendar.DAY_OF_WEEK) + 7 - today.getFirstDayOfWeek()) % 7;
        firstDayOfWeek.add(Calendar.DAY_OF_YEAR, -currentDayOfWeek);

        firstDayOfPreviousWeek.add(Calendar.DAY_OF_YEAR, -currentDayOfWeek - 7);

        final int currentDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
        firstDayOfMonth.add(Calendar.DAY_OF_YEAR, 1 - currentDayOfMonth);

        firstDayOfPreviousMonth.add(Calendar.DAY_OF_YEAR, -currentDayOfMonth);
        final int lastDayOfPreviousMonth = firstDayOfPreviousMonth.get(Calendar.DAY_OF_MONTH);

        firstDayOfPreviousMonth.add(Calendar.DAY_OF_YEAR, 1 - lastDayOfPreviousMonth);

        final int currentDayOfYear = today.get(Calendar.DAY_OF_YEAR);
        firstDayOfYear.add(Calendar.DAY_OF_YEAR, 1 - currentDayOfYear);

        firstDayOfPreviousYear.add(Calendar.DAY_OF_YEAR, 1 - currentDayOfYear);

        // TODAY
        requestOfToday = getRequestCount(today.getTime(), null);

        // YESTERDAY
        requestOfYesterday = getRequestCount(yesterday.getTime(), today.getTime());

        // THIS WEEK
        requestOfThisWeek = getRequestCount(firstDayOfWeek.getTime(), null);

        // LAST WEEK
        requestOfPreviousWeek = getRequestCount(firstDayOfPreviousWeek.getTime(), firstDayOfWeek.getTime());

        // THIS MONTH
        requestOfThisMonth = getRequestCount(firstDayOfMonth.getTime(), null);

        // LAST MONTH
        requestOfPreviousMonth = getRequestCount(firstDayOfPreviousMonth.getTime(), firstDayOfMonth.getTime());

        // THIS YEAR
        requestOfThisYear = getRequestCount(firstDayOfYear.getTime(), null);

        // LAST YEAR
        requestOfPreviousYear = getRequestCount(firstDayOfPreviousYear.getTime(), firstDayOfYear.getTime());

        //Ever
        requestSinceTheBegining = WebServicesInformation.getWsInformationWithName("DDS_Ws").getTotalRequest();


    }

    public FilterDataModel<UserIpAddress> getAllUserIpAddressSinceLast24h00DM() {
        return new FilterDataModel<UserIpAddress>(getFilterUserIpAddress()) {
            @Override
            protected Object getId(UserIpAddress userIpAddress) {
                return userIpAddress.getId();
            }
        };
    }

    private Filter<UserIpAddress> getFilterUserIpAddress() {
        if (filterUserIpAddress == null) {
            UserIpAddressQuery q = new UserIpAddressQuery();
            HQLCriterionsForFilter<UserIpAddress> res = q.getHQLCriterionsForFilter();
            res.addQueryModifier(new QueryModifier<UserIpAddress>() {
                @Override
                public void modifyQuery(HQLQueryBuilder<UserIpAddress> hqlQueryBuilder, Map<String, Object> map) {
                    UserIpAddressQuery q = new UserIpAddressQuery();
                    hqlQueryBuilder.addRestriction(q.requestTime().gtRestriction(new Date(new Date().getTime() - 86400000)));
                }
            });
            this.filterUserIpAddress = new Filter<UserIpAddress>(res);
        }
        return this.filterUserIpAddress;
    }

    public String getSelectedTab() {
        return selectedTab;
    }


    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    public FilterDataModel<UserRequestHistoric> getAllUserRequestHistoricDM() {
        return new FilterDataModel<UserRequestHistoric>(getFilterUserRequestHistoric()) {
            @Override
            protected Object getId(UserRequestHistoric userRequestHistoric) {
                return userRequestHistoric.getId();
            }
        };
    }

    private Filter<UserRequestHistoric> getFilterUserRequestHistoric() {
        if (filterUserRequestHistoric == null) {
            UserRequestHistoricQuery q = new UserRequestHistoricQuery();
            HQLCriterionsForFilter<UserRequestHistoric> res = q.getHQLCriterionsForFilter();
            this.filterUserRequestHistoric = new Filter<UserRequestHistoric>(res);
        }
        return this.filterUserRequestHistoric;
    }

    public List<UserIpAddress> getAllUserIpAddress() {
        List<UserIpAddress> res = UserIpAddress.getAllUsersRequests();
        log.info("res.size() = " + res.size());
        return res;
    }

    public FilterDataModel<WebServicesInformation> getAllWebServicesInformationDM() {
        return new FilterDataModel<WebServicesInformation>(getFilterWebServicesInformation()) {
            @Override
            protected Object getId(WebServicesInformation webServicesInformation) {
                return webServicesInformation.getId();
            }
        };
    }

    private Filter<WebServicesInformation> getFilterWebServicesInformation() {
        if (filterWebServicesInformation == null) {
            WebServicesInformationQuery q = new WebServicesInformationQuery();
            HQLCriterionsForFilter<WebServicesInformation> res = q.getHQLCriterionsForFilter();
            res.addQueryModifier(new QueryModifier<WebServicesInformation>() {
                @Override
                public void modifyQuery(HQLQueryBuilder<WebServicesInformation> hqlQueryBuilder, Map<String, Object> map) {
                    WebServicesInformationQuery q = new WebServicesInformationQuery();
                    hqlQueryBuilder.addRestriction(q.wsName().neqRestriction("DDS_Ws"));
                }
            });
            this.filterWebServicesInformation = new Filter<WebServicesInformation>(res);
        }
        return this.filterWebServicesInformation;
    }

    public int getDDSRequestAuthorizedForOneDay() {
        return WebServicesInformation.getWsInformationWithName("DDS_Ws").getRequestMaximumNumber();
    }


    public int getTotalRequestForUserHistoricIp(UserRequestHistoric userRequestHistoric) {
        return (userRequestHistoric.getFailedRequestNumber() + userRequestHistoric.getRequestNumber());
    }


    public Date getTimeOfFirstRequest() {
        return UserIpAddress.getAllUsersRequests().get(0).getRequestTime();
    }


    public int getRequestOfToday() {
        return requestOfToday;
    }


    public int getRequestOfYesterday() {
        return requestOfYesterday;
    }

    public int getRequestOfThisWeek() {
        return requestOfThisWeek;
    }

    public int getRequestOfPreviousWeek() {
        return requestOfPreviousWeek;
    }


    public int getRequestOfThisMonth() {
        return requestOfThisMonth;
    }

    public int getRequestOfPreviousMonth() {
        return requestOfPreviousMonth;
    }

    public int getRequestOfThisYear() {
        return requestOfThisYear;
    }

    public int getRequestOfPreviousYear() {
        return requestOfPreviousYear;
    }

    public int getRequestSinceTheBegining() {
        return requestSinceTheBegining;
    }


    /**
     * Destroy the Manager bean when the session is over.
     */

    @Destroy
    @Remove
    public void destroy() {
        log.debug("destroy");
    }


}
