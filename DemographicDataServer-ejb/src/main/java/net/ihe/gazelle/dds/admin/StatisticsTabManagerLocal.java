package net.ihe.gazelle.dds.admin;

import javax.ejb.Local;

@Local
public interface StatisticsTabManagerLocal {

    public void destroy();
}
