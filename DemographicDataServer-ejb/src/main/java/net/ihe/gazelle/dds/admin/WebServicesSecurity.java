package net.ihe.gazelle.dds.admin;

import net.ihe.gazelle.dds.model.UserIpAddress;
import net.ihe.gazelle.dds.model.UserIpAddressQuery;
import net.ihe.gazelle.dds.model.UserRequestHistoric;
import net.ihe.gazelle.dds.model.WebServicesInformation;
import net.ihe.gazelle.dds.utils.UserIpAddressQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

/**
 * @author nicolas lefebvre | IHE development team | INRIA
 */

public class WebServicesSecurity {

    public static boolean requestWsPermission(String ip) {
        if (ip == null) {
            return false;
        }
        UserRequestHistoric urh = UserRequestHistoric.findUserRequestHistoric(ip);

        int requestMaximumNumber = ApplicationConfigurationProvider.instance().getWsRequestMaximumNumber();
        long timeInterval = ApplicationConfigurationProvider.instance().getWsTimeInterval();
        long dayInterval = ApplicationConfigurationProvider.instance().getWsDayInterval();
        int maximumRequestPerUser = ApplicationConfigurationProvider.instance().getWsMaximumRequestPerUser();

        if (urh == null) {
            urh = new UserRequestHistoric(ip, new Date(), 0, 0);
        }
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(urh);
        entityManager.flush();

        Date currentTime = new Date();

        Integer userRequestPerDay = 0;

        Integer requestNumberDuringInterval = 0;

        int lastUserId = 0;

        // Verification of the max request number by users per day
        UserIpAddressQuery query = new UserIpAddressQuery();
        if (!urh.isUnlimited()) {
            long longDateLast24hours = new Date().getTime() - dayInterval;
            query.requestTime().gt(new Date(longDateLast24hours));
            query.ipAddress().eq(ip);
            userRequestPerDay = query.getCount();
        }

        // Update the WebServices Information table
        updateWSInformationTable();

        List<WebServicesInformation> wsInformation = WebServicesInformation.getAllWsInformation();
        int j = wsInformation.size() - 1;

        // /////////////Verification of the max requests number per day for each Ws.
        j = wsInformation.size() - 1;
        while (j >= 0) {
            if (wsInformation.get(j).getRequestMaximumNumber() <= wsInformation.get(j).getRequestSinceLastDay()) {
                manageUserRequestHistoric(ip, currentTime, false);
                return false;
            } else {
                j--;
            }
        }

        // Verification of the max requests number per time interval
        if (!urh.isUnlimited()) {
            long longDateLastTimeInterval = new Date().getTime() - timeInterval;
            query = new UserIpAddressQuery();
            query.requestTime().gt(new Date(longDateLastTimeInterval));
            requestNumberDuringInterval = query.getCount();
        }

        WebServicesInformation ws;

        if ((requestNumberDuringInterval < requestMaximumNumber) && (userRequestPerDay < maximumRequestPerUser)) {
            // User initialization
            UserIpAddress user = new UserIpAddress();
            user.setIpAddress(ip);
            user.setRequestTime(currentTime);
            user.setGoogleRequestNumber(0);
            user.setNominatimRequestNumber(0);
            user.setGeonameRequestNumber(0);

            user = UserIpAddress.mergeUserIpAddress(user);
            lastUserId = user.getId();

            // To save the last user Id.
            j = wsInformation.size() - 1;
            while (j >= 0) {
                ws = wsInformation.get(j);
                if (ws.getWsName().equals("DDS_Ws")) {
                    ws.setLastUserIpId(lastUserId);
                    WebServicesInformation.mergeWebServicesInformation(ws);
                }

                j--;
            }

            manageUserRequestHistoric(ip, currentTime, true);
            return true;
        } else {
            manageUserRequestHistoric(ip, currentTime, false);
            return false;
        }

    }

    public static void updateWSInformationTable() {

        long dayInterval = ApplicationConfigurationProvider.instance().getWsDayInterval();

        long longDateLast24hours = new Date().getTime() - dayInterval;

        List<WebServicesInformation> wsInformation = WebServicesInformation.getAllWsInformation();

        Integer nominatimRequest = 0;
        Integer googleRequest = 0;
        Integer geonameRequest = 0;
        Integer ddsWsRequest = 0;

        // ////////Calculate the request number of each Ws since the last 24 hours.
        UserIpAddressQuery query = new UserIpAddressQuery();
        query.requestTime().gt(new Date(longDateLast24hours));
        ddsWsRequest = query.getCount();

        UserIpAddressQueryBuilder queryBuilder = new UserIpAddressQueryBuilder();
        queryBuilder.addRestriction(HQLRestrictions.gt("requestTime", new Date(longDateLast24hours)));
        nominatimRequest = queryBuilder.computeSumOnAttribute("nominatimRequestNumber");
        googleRequest = queryBuilder.computeSumOnAttribute("googleRequestNumber");
        geonameRequest = queryBuilder.computeSumOnAttribute("geonameRequestNumber");

        WebServicesInformation ws;
        int j = wsInformation.size() - 1;
        while (j >= 0) {
            ws = wsInformation.get(j);
            if (wsInformation.get(j).getWsName().equals("DDS_Ws")) {
                ws.setRequestSinceLastDay(ddsWsRequest);
                WebServicesInformation.mergeWebServicesInformation(ws);
            }
            if (ws.getWsName().equals("Nominatim")) {
                ws.setRequestSinceLastDay(nominatimRequest);
                WebServicesInformation.mergeWebServicesInformation(ws);
            }
            if (wsInformation.get(j).getWsName().equals("Google")) {
                ws.setRequestSinceLastDay(googleRequest);
                WebServicesInformation.mergeWebServicesInformation(ws);
            }
            if (wsInformation.get(j).getWsName().equals("Geoname")) {
                ws.setRequestSinceLastDay(geonameRequest);
                WebServicesInformation.mergeWebServicesInformation(ws);
            }

            j--;
        }

    }

    public static void manageUserRequestHistoric(String ip, Date date, boolean requestState) {
        List<UserRequestHistoric> userHistoric = UserRequestHistoric.getAllUserRequestHistoric();
        int i = userHistoric.size() - 1;
        int j = 0;

        UserRequestHistoric user = new UserRequestHistoric();

        while (i >= 0) {
            if (ip.equals(userHistoric.get(i).getIpAddress())) {
                user = userHistoric.get(i);
                user.setLastRequestTime(date);
                j = 1;

                if (requestState) {
                    // The request has not failed
                    user.setRequestNumber(user.getRequestNumber() + 1);
                } else {
                    user.setFailedRequestNumber(user.getFailedRequestNumber() + 1);
                }

                UserRequestHistoric.mergeUserRequestHistoric(user);
            }

            i--;
        }

        // This user is unknown, need to create it in the table
        if (j == 0) {
            user.setIpAddress(ip);
            user.setLastRequestTime(date);

            if (requestState) {
                // The request has not failed
                user.setRequestNumber(1);
                user.setFailedRequestNumber(0);
            } else {
                user.setRequestNumber(0);
                user.setFailedRequestNumber(1);
            }

            UserRequestHistoric.mergeUserRequestHistoric(user);
        }

    }

}
