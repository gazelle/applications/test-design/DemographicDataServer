/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.hl7;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import net.ihe.gazelle.dds.action.SystemConfig;
import net.ihe.gazelle.dds.model.*;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


/**
 * <b>Class Description :  </b>HL7MessageGenerator<br><br>
 * This class provides methods to build HL7 messages for a given message type and HL7 version.
 *
 * @author Nicolas Lefebvre - Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, November 24th
 */

public class Hl7MessageGenerator {

    private static final String SENDING_APPLICATION = "Gazelle";
    private static final String SENDING_FACILITY = "IHE";
    private static final String messageType_A01_231 = "ADT^A01^ADT_A01 (v2.3.1)";
    private static final String messageType_A04_231 = "ADT^A04^ADT_A01 (v2.3.1)";
    private static final String messageType_A28_25 = "ADT^A28^ADT_A05 (v2.5)";
    private static final String A01 = "A01";
    private static final String A04 = "A04";
    private static final String A08 = "A08";
    private static final String ISO = "ISO";
    /**
     * Logger
     */
    @Logger
    private static Log log = Logging.getLog(Hl7MessageGenerator.class);
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);

    public Hl7MessageGenerator() {

    }


    public static Message generate(
            String inMessageType,
            String inHl7Version,
            Patient inPatient,
            SystemConfig configuration,
            String charset) {
        if ((inMessageType == null) || (inHl7Version == null)) {
            log.info("null message type and/or HL7 Version");
            return null;
        } else if ((inMessageType.equals(A01)) && (inHl7Version.equals("v2.3.1"))) {
            return createA01ForV231(A01, inPatient, configuration, charset);
        } else if ((inMessageType.equals(A01)) && (inHl7Version.equals("v2.5"))) {
            return createA01ForV25(A01, inPatient, configuration, charset);
        } else if ((inMessageType.equals(A04)) && (inHl7Version.equals("v2.3.1"))) {
            return createA01ForV231(A04, inPatient, configuration, charset);
        } else if ((inMessageType.equals(A04)) && (inHl7Version.equals("v2.5"))) {
            return createA01ForV25(A04, inPatient, configuration, charset);
        } else if ((inMessageType.equals(A08)) && (inHl7Version.equals("v2.3.1"))) {
            return createA01ForV231(A08, inPatient, configuration, charset);
        } else if ((inMessageType.equals(A08)) && (inHl7Version.equals("v2.5"))) {
            return createA01ForV25(A08, inPatient, configuration, charset);
        } else {
            log.error("message type and/or HL7 version do not match any defined string");
            return null;
        }
    }


    public static Message generate(
            String messagetypeAndVersion,
            Patient inPatient,
            SystemConfig configuration,
            String charset) {

        if (messagetypeAndVersion == null) {
            log.info("null message type");
            return null;
        } else if (messagetypeAndVersion.equals(messageType_A01_231)) {
            return createA01ForV231(
                    A01,
                    inPatient,
                    configuration,
                    charset);
        } else if (messagetypeAndVersion.equals(messageType_A04_231)) {
            return createA01ForV231(
                    A04,
                    inPatient,
                    configuration,
                    charset);
        } else if (messagetypeAndVersion.equals(messageType_A28_25)) {
            return createA28ForV25(
                    "A28",
                    inPatient,
                    configuration,
                    charset);
        } else {
            log.error("message type does not match any defined string");
            return null;
        }
    }

    /**
     * Create Message with structure ADT_A01 using version 2.3.1 of HL7
     *
     * @param triggerEvent
     * @param inPatient
     * @param targetApplication
     * @param targetFacility
     * @return
     */
    private static ca.uhn.hl7v2.model.v231.message.ADT_A01 createA01ForV231(
            String inTriggerEvent,
            Patient inPatient,
            SystemConfig inConfiguration,
            String charset) {
        ca.uhn.hl7v2.model.v231.message.ADT_A01 message = new ca.uhn.hl7v2.model.v231.message.ADT_A01();
        // populate the MSH Segment
        ca.uhn.hl7v2.model.v231.segment.MSH mshSegment = message.getMSH();
        Hl7MessageGenerator hmg = new Hl7MessageGenerator();
        hmg.fillMSHSegmentForV231(
                inPatient,
                mshSegment,
                "ADT",
                inTriggerEvent,
                "ADT_A01",
                inConfiguration.getReceivingApplication(),
                inConfiguration.getReceivingFacility(),
                charset);
        // populate the PID Segment
        ca.uhn.hl7v2.model.v231.segment.PID pidSegment = message.getPID();
        List<AssigningAuthority> authorities = new ArrayList<AssigningAuthority>();

        authorities.add(inConfiguration.getAuthority());

        Hl7MessageGenerator.fillPIDSegmentForV231(inPatient, pidSegment, authorities);

        //populat the EVN Segment
        ca.uhn.hl7v2.model.v231.segment.EVN evnSegment = message.getEVN();
        hmg = new Hl7MessageGenerator();
        hmg.fillEVNSegmentFor231(evnSegment, inTriggerEvent);
        //populate the PV1 Segment
        ca.uhn.hl7v2.model.v231.segment.PV1 pv1Segment = message.getPV1();
        Hl7MessageGenerator.fillPV1SegmentFor231(pv1Segment, inTriggerEvent);
        return message;
    }

    /**
     * Create Message with structure ADT_A01 using version 2.5 of HL7
     *
     * @param triggerEvent
     * @param inPatient
     * @param targetApplication
     * @param targetFacility
     * @return
     */
    private static ca.uhn.hl7v2.model.v25.message.ADT_A01 createA01ForV25(String inTriggerEvent, Patient inPatient, SystemConfig inConfiguration, String charset) {
        ca.uhn.hl7v2.model.v25.message.ADT_A01 message = new ca.uhn.hl7v2.model.v25.message.ADT_A01();
        // populate the MSH Segment
        ca.uhn.hl7v2.model.v25.segment.MSH mshSegment = message.getMSH();
        Hl7MessageGenerator hmg = new Hl7MessageGenerator();
        hmg.fillMSHSegmentForV25(inPatient, mshSegment, "ADT", inTriggerEvent, "ADT_A01", inConfiguration.getReceivingApplication(), inConfiguration.getReceivingFacility(), charset);
        // populate the PID Segment
        ca.uhn.hl7v2.model.v25.segment.PID pidSegment = message.getPID();
        List<AssigningAuthority> authorities = new ArrayList<AssigningAuthority>();

        authorities.add(inConfiguration.getAuthority());

        Hl7MessageGenerator.fillPIDSegmentForV25(inPatient, pidSegment, authorities);

        //populat the EVN Segment
        ca.uhn.hl7v2.model.v25.segment.EVN evnSegment = message.getEVN();
        hmg = new Hl7MessageGenerator();
        hmg.fillEVNSegmentFor25(evnSegment, inTriggerEvent);
        //populate the PV1 Segment
        ca.uhn.hl7v2.model.v25.segment.PV1 pv1Segment = message.getPV1();
        Hl7MessageGenerator.fillPV1SegmentFor25(pv1Segment, inTriggerEvent);
        return message;
    }


    /**
     * Create Message with structure ADT^A28^ADT_A05 using version 2.5 of HL7
     *
     * @param triggerEvent
     * @param inPatient
     * @param targetApplication
     * @param targetFacility
     * @return
     */
    private static ca.uhn.hl7v2.model.v25.message.ADT_A05 createA28ForV25(
            String inTriggerEvent,
            Patient inPatient,
            SystemConfig inConfiguration,
            String charset) {
        ca.uhn.hl7v2.model.v25.message.ADT_A05 message = new ca.uhn.hl7v2.model.v25.message.ADT_A05();
        // populate the MSH Segment
        ca.uhn.hl7v2.model.v25.segment.MSH mshSegment = message.getMSH();
        Hl7MessageGenerator hmg = new Hl7MessageGenerator();
        hmg.fillMSHSegmentForV25(
                inPatient,
                mshSegment,
                "ADT",
                inTriggerEvent,
                "ADT_A05",
                inConfiguration.getReceivingApplication(),
                inConfiguration.getReceivingFacility(),
                charset);

        // populate the PID Segment
        ca.uhn.hl7v2.model.v25.segment.PID pidSegment = message.getPID();
        List<AssigningAuthority> authorities = new ArrayList<AssigningAuthority>();

        authorities.add(inConfiguration.getAuthority());

        Hl7MessageGenerator.fillPIDSegmentForV25(inPatient, pidSegment, authorities);

        //populat the EVN Segment
        ca.uhn.hl7v2.model.v25.segment.EVN evnSegment = message.getEVN();
        hmg = new Hl7MessageGenerator();
        hmg.fillEVNSegmentFor25(evnSegment, inTriggerEvent);

        //populate the PV1 Segment
        ca.uhn.hl7v2.model.v25.segment.PV1 pv1Segment = message.getPV1();
        Hl7MessageGenerator.fillPV1SegmentFor25(
                pv1Segment,
                inTriggerEvent);

        return message;
    }

    /**
     * Fill the PID segment for the HL7v2.3.1 version
     *
     * @param mshSegment
     * @param messageType
     * @param triggerEvent
     * @param messageStructure
     */
    private static void fillPIDSegmentForV231(Patient patient, ca.uhn.hl7v2.model.v231.segment.PID pid, List<AssigningAuthority> inAuthorities) {

        try {
            if (patient == null) {
                log.error("the given patient is null");
                return;
            }
            if (patient.getPerson() != null) {

                if (patient.getPerson().getLastName() == null) {
                    pid.getPatientName(0).getFamilyLastName().getFamilyName().setValue(patient.getPerson().getLastAlternativeName());
                } else if (patient.getPerson().getLastName().getValue() != null) {
                    pid.getPatientName(0).getFamilyLastName().getFamilyName().setValue(patient.getPerson().getLastName().getValue());
                }

                if (patient.getPerson().getFirstNameSex() == null) {
                    pid.getPatientName(0).getGivenName().setValue(patient.getPerson().getFirstAlternativeName());
                } else if (patient.getPerson().getFirstNameSex().getFirstName() != null) {
                    pid.getPatientName(0).getGivenName().setValue(patient.getPerson().getFirstNameSex().getFirstName());
                }


                //for the Other Name :
                if (patient.getPerson().getOtherNameList() != null
                        && !patient.getPerson().getOtherNameList().isEmpty()) {
                    List<OtherName> otherNames = patient.getPerson().getOtherNameList();
                    for (int i = 0; i < otherNames.size(); i++) {
                        if (otherNames.get(i).getLastName() != null && otherNames.get(i).getLastName().getValue() != null
                                && !otherNames.get(i).getLastName().getValue().isEmpty()) {
                            pid.getPatientName(i).getFamilyLastName().getFamilyName().setValue(
                                    otherNames.get(i).getLastName().getValue());
                        }

                        if (otherNames.get(i).getFirstNameSex() != null
                                && otherNames.get(i).getFirstNameSex().getFirstName() != null
                                && !otherNames.get(i).getFirstNameSex().getFirstName().isEmpty()) {
                            pid.getPatientName(i).getGivenName().setValue(otherNames.get(i).getFirstNameSex().getFirstName());
                        }

                        if (otherNames.get(i).getNameType() != null && !otherNames.get(i).getNameType().isEmpty()) {
                            pid.getPatientName(i).getNameTypeCode().setValue(otherNames.get(i).getNameType());
                        }
                    }
                }

                //Dead Patient Or Not
                if (patient.getPerson().getDateOfDeath() != null) {
                    pid.getPatientDeathIndicator().setValue("Y");
                    pid.getPatientDeathDateAndTime().getTimeOfAnEvent().setValue(
                            getDateOfBirthAsString(patient.getPerson().getDateOfDeath()));
                }


                //Marital Status
                if (patient.getPerson().getMaritalStatus() != null && !patient.getPerson().getMaritalStatus().isEmpty()) {
                    pid.getMaritalStatus().getIdentifier().setValue(patient.getPerson().getMaritalStatus());
                }

                if (patient.getPerson().getMotherMaidenName() != null) {
                    pid.getMotherSMaidenName(0).getFamilyLastName().getFamilyName().setValue(
                            patient.getPerson().getMotherMaidenName().getValue());
                    pid.getMotherSMaidenName(0).getNameTypeCode().setValue("L");
                }

                if (patient.getPerson().getDateOfBirth() != null) {
                    pid.getDateTimeOfBirth().getTimeOfAnEvent().setValue(
                            getDateOfBirthAsString(patient.getPerson().getDateOfBirth()));
                }

                if (patient.getPerson().getRace() != null) {
                    pid.getRace(0).getText().setValue(patient.getPerson().getRace().getCode());
                }

                if (patient.getPerson().getReligion() != null) {
                    pid.getReligion().getText().setValue(patient.getPerson().getReligion().getCode());
                }

                if (patient.getPerson().getSex() != null) {
                    pid.getSex().setValue(patient.getPerson().getSex().getCode());
                }
            }

            if (patient.getAddress() != null && patient.getAddress().size() > 0) {
                if (patient.getAddress().get(0).getStreet() != null) {
                    pid.getPatientAddress(0).getStreetAddress().setValue(patient.getAddress().get(0).getStreet().getValue());
                }
                if (patient.getAddress().get(0).getCountry() != null) {
                    pid.getPatientAddress(0).getCountry().setValue(getCountryIso3(patient.getAddress().get(0).getCountry().getIso()));
                }
                if (patient.getAddress().get(0).getCity() != null) {
                    pid.getPatientAddress(0).getCity().setValue(patient.getAddress().get(0).getCity().getValue());
                }
                if (patient.getAddress().get(0).getPostalCode() != null) {
                    pid.getPatientAddress(0).getZipOrPostalCode().setValue(patient.getAddress().get(0).getPostalCode());
                }
                if (patient.getAddress().get(0).getState() != null) {
                    pid.getPatientAddress(0).getStateOrProvince().setValue(patient.getAddress().get(0).getState().getCode());
                }
            }


            if (inAuthorities == null) {
                return;
            }
            int index = 0;
            for (AssigningAuthority authority : inAuthorities) {
                pid.getPatientIdentifierList(index).getID().setValue(authority.getKeyword() + "-" + patient.getId().toString());
                pid.getPatientIdentifierList(index).getAssigningAuthority().getUniversalID().setValue(authority.getRootOID());
                pid.getPatientIdentifierList(index).getAssigningAuthority().getUniversalIDType().setValue(ISO);
                pid.getPatientIdentifierList(index).getAssigningAuthority().getNamespaceID().setValue(authority.getKeyword());
                index++;
            }

            if (patient.getNationalPatientIdentifier() != null) {
                pid.getPatientIdentifierList(index).parse(patient.getNationalPatientIdentifier());
            }


            //If NewBorn
            if (patient.getMotherPatient() != null) {
                index = 0;
                for (AssigningAuthority authority : inAuthorities) {
                    pid.getMotherSIdentifier(index).getID().setValue(authority.getKeyword() + "-" + patient.getMotherPatient().getId().toString());
                    pid.getMotherSIdentifier(index).getAssigningAuthority().getUniversalID().setValue(authority.getRootOID());
                    pid.getMotherSIdentifier(index).getAssigningAuthority().getUniversalIDType().setValue(ISO);
                    pid.getMotherSIdentifier(index).getAssigningAuthority().getNamespaceID().setValue(authority.getKeyword());
                    index++;
                }

                //For the national Patient Identifier
                if (patient.getMotherPatient().getNationalPatientIdentifier() != null) {
                    pid.getMotherSIdentifier(index).parse(patient.getMotherPatient().getNationalPatientIdentifier());
                }
            }


        } catch (DataTypeException e) {
            log.error("Data Exception : ", e.getMessage());
        } catch (HL7Exception e) {
            log.error("HL7Exception : ", e.getMessage());
        }

    }

    /**
     * Fill the PV1 Segment for the HL7v2.3.1 version
     *
     * @param inTriggerEvent TODO
     */
    private static void fillPV1SegmentFor231(ca.uhn.hl7v2.model.v231.segment.PV1 segment, String inTriggerEvent) {
        try {
            if (inTriggerEvent.equals(A01)) {
                segment.getPatientClass().setValue("I");
                // inpatient
            } else if ((inTriggerEvent.equals(A04)) || (inTriggerEvent.equals(A08))) {
                segment.getPatientClass().setValue("O");
                // outpatient
            }
        } catch (DataTypeException e) {
            log.error("Error when filling PV1 Segment : " + e.getMessage());
        }
    }

    /**
     * Fill the PID segment for the HL7v2.5 version
     *
     * @param mshSegment
     * @param messageType
     * @param triggerEvent
     * @param messageStructure
     */
    private static void fillPIDSegmentForV25(Patient patient, ca.uhn.hl7v2.model.v25.segment.PID pid, List<AssigningAuthority> inAuthorities) {
        try {
            if (patient == null) {
                log.error("the given patient is null");
                return;
            }


            if (patient.getPerson() != null) {
                //for the Legal Name :
                if (patient.getPerson().getLastName() == null) {
                    pid.getPatientName(0).getFamilyName().getSurname().setValue(patient.getPerson().getLastAlternativeName());
                } else if (patient.getPerson().getLastName().getValue() != null) {
                    pid.getPatientName(0).getFamilyName().getSurname().setValue(patient.getPerson().getLastName().getValue());
                }

                if (patient.getPerson().getFirstNameSex() == null) {
                    pid.getPatientName(0).getGivenName().setValue(patient.getPerson().getFirstAlternativeName());
                } else if (patient.getPerson().getFirstNameSex().getFirstName() != null) {

                    pid.getPatientName(0).getGivenName().setValue(patient.getPerson().getFirstNameSex().getFirstName());
                }

                if (patient.getPerson().getNameType() != null && !patient.getPerson().getNameType().isEmpty()) {
                    pid.getPatientName(0).getNameTypeCode().setValue(patient.getPerson().getNameType());
                }

                if (patient.getPerson().getOtherFirstNameSexList() != null
                        && patient.getPerson().getOtherFirstNameSexList().size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    for (FirstNameSex firstNameSex : patient.getPerson().getOtherFirstNameSexList()) {
                        sb.append(" ");
                        sb.append(firstNameSex.getFirstName());
                    }
                    pid.getPatientName(0).getSecondAndFurtherGivenNamesOrInitialsThereof().
                            setValue(sb.toString());
                }

                //for the Other Name :
                if (patient.getPerson().getOtherNameList() != null
                        && !patient.getPerson().getOtherNameList().isEmpty()) {
                    List<OtherName> otherNames = patient.getPerson().getOtherNameList();
                    for (int i = 0; i < otherNames.size(); i++) {
                        if (otherNames.get(i).getLastName() != null && otherNames.get(i).getLastName().getValue() != null
                                && !otherNames.get(i).getLastName().getValue().isEmpty()) {
                            pid.getPatientName(i).getFamilyName().getSurname().setValue(
                                    otherNames.get(i).getLastName().getValue());
                        }

                        if (otherNames.get(i).getFirstNameSex() != null
                                && otherNames.get(i).getFirstNameSex().getFirstName() != null
                                && !otherNames.get(i).getFirstNameSex().getFirstName().isEmpty()) {
                            pid.getPatientName(i).getGivenName().setValue(otherNames.get(i).getFirstNameSex().getFirstName());
                        }

                        if (otherNames.get(i).getNameType() != null && !otherNames.get(i).getNameType().isEmpty()) {
                            pid.getPatientName(i).getNameTypeCode().setValue(otherNames.get(i).getNameType());
                        }
                    }
                }


                //Dead Patient Or Not
                if (patient.getPerson().getDateOfDeath() != null) {
                    pid.getPatientDeathIndicator().setValue("Y");
                    pid.getPatientDeathDateAndTime().getTime().setValue(
                            getDateOfBirthAsString(patient.getPerson().getDateOfDeath()));
                }


                //Marital Status
                if (patient.getPerson().getMaritalStatus() != null && !patient.getPerson().getMaritalStatus().isEmpty()) {
                    pid.getMaritalStatus().getIdentifier().setValue(patient.getPerson().getMaritalStatus());
                }


                if (patient.getPerson().getMotherMaidenName() != null) {
                    pid.getMotherSMaidenName(0).getFamilyName().getSurname().setValue(
                            patient.getPerson().getMotherMaidenName().getValue());
                    pid.getMotherSMaidenName(0).getNameTypeCode().setValue("L");
                }


                if (patient.getPerson().getDateOfBirth() != null) {
                    pid.getDateTimeOfBirth().getTime().setValue(getDateOfBirthAsString(patient.getPerson().getDateOfBirth()));
                }


                if (patient.getPerson().getRace() != null) {
                    pid.getRace(0).getText().setValue(patient.getPerson().getRace().getCode());
                }


                if (patient.getPerson().getReligion() != null) {
                    pid.getReligion().getText().setValue(patient.getPerson().getReligion().getCode());
                }


                if (patient.getPerson().getSex() != null) {
                    pid.getAdministrativeSex().setValue(patient.getPerson().getSex().getCode());
                }
            }

            if (patient.getAddress() != null && patient.getAddress().size() > 0) {
                if (patient.getAddress().get(0).getStreet() != null) {
                    pid.getPatientAddress(0).getStreetAddress().getStreetName().setValue(patient.getAddress().get(0).getStreet().getValue());
                }
                if (patient.getAddress().get(0).getCountry() != null) {
                    pid.getPatientAddress(0).getCountry().setValue(getCountryIso3(patient.getAddress().get(0).getCountry().getIso()));
                }
                if (patient.getAddress().get(0).getCity() != null) {
                    pid.getPatientAddress(0).getCity().setValue(patient.getAddress().get(0).getCity().getValue());
                }
                if (patient.getAddress().get(0).getPostalCode() != null) {
                    pid.getPatientAddress(0).getZipOrPostalCode().setValue(patient.getAddress().get(0).getPostalCode());
                }
                if (patient.getAddress().get(0).getState() != null) {
                    pid.getPatientAddress(0).getStateOrProvince().setValue(patient.getAddress().get(0).getState().getCode());
                }
            }


            if (inAuthorities == null) {
                return;
            }

            int index = 0;
            for (AssigningAuthority authority : inAuthorities) {
                pid.getPatientIdentifierList(index).getIDNumber().setValue(authority.getKeyword() + "-" + patient.getId().toString());
                pid.getPatientIdentifierList(index).getAssigningAuthority().getUniversalID().setValue(authority.getRootOID());
                pid.getPatientIdentifierList(index).getAssigningAuthority().getUniversalIDType().setValue(ISO);
                pid.getPatientIdentifierList(index).getAssigningAuthority().getNamespaceID().setValue(authority.getKeyword());
                index++;
            }

            //For the national Patient Identifier
            if (patient.getNationalPatientIdentifier() != null) {
                pid.getPatientIdentifierList(index).parse(patient.getNationalPatientIdentifier());
            }


            //If NewBorn
            if (patient.getMotherPatient() != null) {
                index = 0;
                for (AssigningAuthority authority : inAuthorities) {
                    pid.getMotherSIdentifier(index).getIDNumber().setValue(
                            authority.getKeyword() + "-" + patient.getMotherPatient().getId().toString());
                    pid.getMotherSIdentifier(index).getAssigningAuthority().getUniversalID().setValue(authority.getRootOID());
                    pid.getMotherSIdentifier(index).getAssigningAuthority().getUniversalIDType().setValue(ISO);
                    pid.getMotherSIdentifier(index).getAssigningAuthority().getNamespaceID().setValue(authority.getKeyword());
                    index++;
                }

                //For the national Patient Identifier
                if (patient.getMotherPatient().getNationalPatientIdentifier() != null) {
                    pid.getMotherSIdentifier(index).parse(patient.getMotherPatient().getNationalPatientIdentifier());
                }
            }


        } catch (DataTypeException e) {
            log.error("Data Exception : " + e.getMessage());
        } catch (HL7Exception e) {
            log.error("HL7Exception: " + e.getMessage());
        }
    }

    /**
     * Fill the PV1 Segment for the HL7v2.5 version
     *
     * @param inTriggerEvent TODO
     */
    private static void fillPV1SegmentFor25(ca.uhn.hl7v2.model.v25.segment.PV1 segment, String inTriggerEvent) {
        try {
            if (inTriggerEvent.equals(A01)) {
                segment.getPatientClass().setValue("I");
                // inpatient
            } else if ((inTriggerEvent.equals(A04)) || (inTriggerEvent.equals(A08))) {
                segment.getPatientClass().setValue("O");
                // outpatient
            } else if (inTriggerEvent.equals("A28")) {
                segment.getPatientClass().setValue("N");
            }
        } catch (DataTypeException e) {
            log.error("Error when filling PV1 Segment : " + e.getMessage());
        }
    }

    private static String getDateOfBirthAsString(Calendar inDate) {
        final String DATE_FORMAT_NOW = "yyyyMMdd";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        if (inDate != null) {
            return sdf.format(inDate.getTime());
        } else {
            return null;
        }
    }

    private static String getCountryIso3(String countryCodeIso) {
        CountryCodeQuery query = new CountryCodeQuery();
        query.iso().eq(countryCodeIso);
        CountryCode countryCode = query.getUniqueResult();
        if (countryCode != null) {
            return countryCode.getIso3();
        } else {
            return null;
        }
    }

    /**
     * Fill the MSH segment for the HL7v2.3.1 version
     *
     * @param mshSegment
     * @param messageType
     * @param triggerEvent
     * @param messageStructure
     */
    private void fillMSHSegmentForV231(Patient patient, ca.uhn.hl7v2.model.v231.segment.MSH mshSegment, String messageType, String triggerEvent, String messageStructure, String targetApplication, String targetFacility, String charset) {
        try {
            mshSegment.getFieldSeparator().setValue("|");
            mshSegment.getEncodingCharacters().setValue("^~\\&");
            mshSegment.getDateTimeOfMessage().getTimeOfAnEvent().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getSendingApplication().getNamespaceID().setValue(SENDING_APPLICATION);
            mshSegment.getSendingFacility().getNamespaceID().setValue(SENDING_FACILITY);
            mshSegment.getReceivingApplication().getNamespaceID().setValue(targetApplication);
            mshSegment.getReceivingFacility().getNamespaceID().setValue(targetFacility);
            mshSegment.getMessageControlID().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getMessageType().getMessageType().setValue(messageType);
            mshSegment.getMessageType().getTriggerEvent().setValue(triggerEvent);
            mshSegment.getMessageType().getMessageStructure().setValue(messageStructure);
            mshSegment.getProcessingID().getProcessingID().setValue("P");
            mshSegment.getVersionID().getVersionID().setValue("2.3.1");

            if (patient.getCountry() != null) {
                mshSegment.getCountryCode().setValue(getCountryIso3(patient.getCountry().getIso()));
            }

            if (charset != null && !charset.contains("ASCII")) {
                try {
                    mshSegment.getCharacterSet(0).setValue(charset);
                } catch (HL7Exception e) {
                    log.error("" + e.getMessage());
                }
            }
        } catch (DataTypeException e) {
            log.error("" + e.getMessage());
        }

    }

    /**
     * Fill the EVN segment for the HL7v2.3.1 version
     *
     * @param evnSegment
     */
    private void fillEVNSegmentFor231(ca.uhn.hl7v2.model.v231.segment.EVN evnSegment, String inTriggerEvent) {
        try {
            evnSegment.getRecordedDateTime().getTimeOfAnEvent().setValue(sdf.format(Calendar.getInstance().getTime()));
            evnSegment.getEventOccurred().getTimeOfAnEvent().setValue(sdf.format(Calendar.getInstance().getTime()));

        } catch (DataTypeException e) {
            log.error("Error when filling EVN segment");
        }
    }

    /**
     * Fill the MSH segment for the HL7v2.5 version
     *
     * @param mshSegment
     * @param messageType
     * @param triggerEvent
     * @param messageStructure
     */
    private void fillMSHSegmentForV25(Patient patient, ca.uhn.hl7v2.model.v25.segment.MSH mshSegment, String messageType, String triggerEvent, String messageStructure, String targetApplication, String targetFacility, String charset) {
        try {
            mshSegment.getFieldSeparator().setValue("|");
            mshSegment.getEncodingCharacters().setValue("^~\\&");
            mshSegment.getDateTimeOfMessage().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getSendingApplication().getNamespaceID().setValue(SENDING_APPLICATION);
            mshSegment.getSendingFacility().getNamespaceID().setValue(SENDING_FACILITY);
            mshSegment.getReceivingApplication().getNamespaceID().setValue(targetApplication);
            mshSegment.getReceivingFacility().getNamespaceID().setValue(targetFacility);
            mshSegment.getSequenceNumber().setValue("1");
            mshSegment.getMessageControlID().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getMessageType().getMessageCode().setValue(messageType);
            mshSegment.getMessageType().getTriggerEvent().setValue(triggerEvent);
            mshSegment.getMessageType().getMessageStructure().setValue(messageStructure);
            mshSegment.getProcessingID().getProcessingID().setValue("P");
            mshSegment.getVersionID().getVersionID().setValue("2.5");

            if (patient.getCountry() != null) {
                mshSegment.getCountryCode().setValue(getCountryIso3(patient.getCountry().getIso()));
            }

            try {
                mshSegment.getCharacterSet(0).setValue(charset);
            } catch (HL7Exception e) {
                log.error("" + e.getMessage());
            }
        } catch (DataTypeException e) {
            log.error("" + e.getMessage());
        }
    }

    /**
     * Fill the EVN segment for the HL7v2.5 version
     *
     * @param evnSegment
     */
    private void fillEVNSegmentFor25(ca.uhn.hl7v2.model.v25.segment.EVN evnSegment, String inTriggerEvent) {
        try {
            evnSegment.getRecordedDateTime().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));
            evnSegment.getEventOccurred().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));

        } catch (DataTypeException e) {
            log.error("Error when filling EVN segment");
        }
    }


}
