package net.ihe.gazelle.dds.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@MappedSuperclass
public abstract class AuditModule {

    @Column(name = "last_changed")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastChanged;

    @Column(name = "last_modifier")
    private String lastModifier;

    public AuditModule() {

    }

    public Date getLastChanged() {
        return lastChanged;
    }

    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

}
