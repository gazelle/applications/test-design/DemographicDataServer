/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;

/**
 * <b>Class Description :  </b>CharacterEncoding
 * This class describes a CharacterEncoding object. It corresponds to a CharacterEncoding generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 *
 * Patient object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the characterEncoding</li>
 * <li><b>value</b> : Object corresponding to the characterEncoding</li>
 * </ul>
 * <b>Example of CharacterEncoding</b> : CharacterEncoding(id, value) = (1, ASCII)
 *
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */

@Entity
@Name("characterEncoding")
@Table(name = "dds_character_encoding", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_character_encoding_sequence", sequenceName = "dds_character_encoding_id_seq", allocationSize = 1)

public class CharacterEncoding extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450004335507896760L;


    //	Attributes (existing in database as a column)

    /**
     * Id of this CharacterEncoding object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_character_encoding_sequence")
    private Integer id;

    /**
     * The value of the carachterEncoding  to describe a language (eg. ASCCI)
     */
    @Column(name = "value")
    private String value;

    //	Constructors

    public CharacterEncoding() {

    }

    public CharacterEncoding(String value) {
        this.value = value;
    }

    public CharacterEncoding(CharacterEncoding characterEncoding) {
        if (characterEncoding.getValue() != null) {
            this.value = characterEncoding.getValue();
        }
    }


    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CharacterEncoding other = (CharacterEncoding) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

}
