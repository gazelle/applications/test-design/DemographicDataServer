/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.model;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>Iso3166CountryCode
 * <p>
 * This class describes the Iso3166CountryCode object, used by the Gazelle application. A Iso3166CountryCode is a country, respecting the iso standards. ISO 3166 is a three-part geographic coding
 * standard for coding the names of countries and dependent areas, and the principal subdivisions thereof, published by the International Organization for Standardization (ISO). The official name is
 * Codes for the representation of names of countries and their subdivisions.
 * <p>
 * The ISO 3166 standard is maintained by the ISO 3166 Maintenance Agency (ISO 3166/MA), whose address is at the ISO central office in Geneva.
 * <p>
 * This class belongs to the Users module.
 * <p>
 * Iso3166CountryCode possesses the following attributes :
 * <ul>
 * <li><b>iso</b> : Iso3166CountryCode id</li>
 * <li><b>name</b> : Name of the country</li>
 * <li><b>printableName</b> : printableName of the country</li>
 * <li><b>iso3</b> : iso3 id (not a primary key, but a 3-chars code)</li>
 * <li><b>numcode</b> : Integer code for that country</li>
 * <li><b>ec</b> : Flag indicating if the country is in the European community</li>
 * <li><b>flagUrl</b> :URL to get the image of that country (flag icon)</li>
 * </ul>
 * <b>Example of Iso3166CountryCode</b> : ('US', 'UNITED STATES', 'United States of America', '/img/flag/usa.jpg);
 *
 * @author Nicolas Lefebvre | Abderrazek boufahja | Samii Ayed / INRIA Rennes IHE development Project
 * @version 2.0 - 2010, November 10th
 */

@XmlRootElement(name = "CountryCode")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Name("countryCode")
@Table(name = "dds_country_code", schema = "public")
public class CountryCode implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450977771541256760L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @XmlElement(name = "id")
    private Integer id;

    @Column(name = "country_code_iso", unique = true, nullable = false, length = 2)
    @NotNull
    @Size(max = 2)
    @XmlElement(name = "iso")
    private String iso;

    @Column(name = "name", nullable = false, length = 80)
    @NotNull
    @Size(max = 80)
    @XmlElement(name = "name")
    private String name;

    @Column(name = "printable_name", nullable = false, length = 80)
    @NotNull
    @Size(max = 80)
    @XmlElement(name = "printableName")
    private String printableName;

    @Column(name = "iso3", length = 3)
    @Size(max = 3)
    @XmlElement(name = "iso3")
    private String iso3;

    @Column(name = "numcode")
    @XmlElement(name = "numcode")
    private Integer numcode;

    @Column(name = "ec")
    @XmlElement(name = "ec")
    private Boolean ec;

    @Column(name = "flag_url")
    @XmlElement(name = "flagUrl")
    private String flagUrl;

    @Column(name = "is_last_name_sexed")
    @XmlElement(name = "isLastNameSexed")
    private Boolean isLastNameSexed;

    @Column(name = "country_oid")
    @XmlElement(name = "countryOID")
    private String countryOID;

    @Column(name = "insee_code")
    @XmlElement(name = "inseeCode")
    private String inseeCode;

    /**
     * The CharacterSetEncoding object corresponding to this country
     */
    @ManyToOne
    @XmlElement(name = "characterSetEncodingReference")
    @JoinColumn(name = "character_set_encoding_id_reference")
    private CharacterSetEncoding characterSetEncodingReference;

    /**
     * Language for this country
     */
    @ManyToMany
    @JoinTable(name = "dds_country_code_language", joinColumns = @JoinColumn(name = "country_code_id"), inverseJoinColumns = @JoinColumn(name = "language_id"))
    private List<Language> language;

    /**
     * CharacterSetEncoding aivailable for this country
     */
    @ManyToMany
    @JoinTable(name = "dds_country_code_character_set", joinColumns = @JoinColumn(name = "country_code_id"), inverseJoinColumns = @JoinColumn(name = "character_set_encoding_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "character_set_encoding_id", "country_code_id"}))
    private List<CharacterSetEncoding> characterSetEncodingList;

    @Column(name = "generate_religion")
    @XmlElement(name = "generateReligion")
    private Boolean generateReligion;

    @Column(name = "generate_race")
    @XmlElement(name = "generateRace")
    private Boolean generateRace;

    @Column(name = "min_number_of_patient_middle_name")
    @XmlElement(name = "minNumberOfPatientMiddleName")
    private Integer minNumberOfPatientMiddleName;

    @Column(name = "max_number_of_patient_middle_name")
    @XmlElement(name = "maxNumberOfPatientMiddleName")
    private Integer maxNumberOfPatientMiddleName;


    @Column(name = "is_usable")
    private Boolean isUsable = false;

    /**
     * used as namespaceID for patient identifiers
     */
    @Column(name = "cx_4_code", nullable = false, length = 80)
    @NotNull
    @Size(max = 80)
    private String cx_4_code;

    // Constructors
    public CountryCode() {
    }

    public CountryCode(Integer id, String iso, String name, String printableName, String iso3, Integer numcode,
                       Boolean ec, String flagUrl, Boolean isLastNameSexed, CharacterSetEncoding characterSetEncodingReference,
                       List<Language> language, List<CharacterSetEncoding> characterSetEncoding, String inseeCode) {
        super();
        this.id = id;
        this.iso = iso;
        this.name = name;
        this.printableName = printableName;
        this.iso3 = iso3;
        this.numcode = numcode;
        this.ec = ec;
        this.flagUrl = flagUrl;
        this.isLastNameSexed = isLastNameSexed;
        this.characterSetEncodingReference = characterSetEncodingReference;
        this.language = language;
        this.characterSetEncodingList = characterSetEncoding;
        this.inseeCode = inseeCode;
    }

    public CountryCode(CountryCode oldIso) {
        if (oldIso != null) {
            if (oldIso.getIso() != null) {
                this.iso = oldIso.getIso();
            }
            if (oldIso.getName() != null) {
                this.name = oldIso.getName();
            }
            if (oldIso.getPrintableName() != null) {
                this.printableName = oldIso.getPrintableName();
            }
            if (oldIso.getIso3() != null) {
                this.iso3 = oldIso.getIso3();
            }
            if (oldIso.getNumcode() != null) {
                this.numcode = oldIso.getNumcode();
            }
            if (oldIso.getEc() != null) {
                this.ec = oldIso.getEc();
            }
            if (oldIso.getFlagUrl() != null) {
                this.flagUrl = oldIso.flagUrl;
            }
            if (oldIso.getIsLastNameSexed() != null) {
                this.isLastNameSexed = oldIso.getIsLastNameSexed();
            }
        }

    }

    public static List<CountryCode> getListCountryCode() {
        CountryCodeQuery query = new CountryCodeQuery();
        return query.getList();
    }

    public static List<CountryCode> getListOfUsableCountryCode() {
        CountryCodeQuery query = new CountryCodeQuery();
        query.printableName().order(true);
        query.isUsable().eq(true);
        return query.getListDistinctOrdered();
    }

    public static CountryCode getCountryByCountryCode(String countryCode) {
        CountryCodeQuery query = new CountryCodeQuery();
        query.iso().eq(countryCode);
        return query.getUniqueResult();
    }

    public static List<String> getcountryLanguageByCountryCode(String countryCodeString) {
        CountryCode inCountryCode = CountryCode.getCountryByCountryCode(countryCodeString);
        List<String> result = new ArrayList<String>();
        for (Language inLanguage : inCountryCode.getLanguage()) {
            result.add(inLanguage.getIso_639_1());
        }
        return result;
    }

    public static String getCountryCodeKeyword(String word) {
        CountryCode countryCode = getCountryCodeByKeyword(word);
        if (countryCode != null) {
            return countryCode.getIso();
        } else {
            return null;
        }
    }

    public static CountryCode getCountryCodeByKeyword(String word) {
        if (word == null || word.isEmpty()) {
            return null;
        } else {
            String wordTmp = word.trim();
            CountryCodeQuery query = new CountryCodeQuery();
            query.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("iso", wordTmp), HQLRestrictions.eq("iso3", wordTmp),
                    HQLRestrictions.eq("name", wordTmp), HQLRestrictions.eq("printableName", wordTmp)));
            return query.getUniqueResult();
        }
    }

    public static List<CountryCode> getCountryCodeFiltered(String iso) {
        CountryCodeQuery query = new CountryCodeQuery();
        if (iso != null && !iso.isEmpty()) {
            query.iso().eq(iso);
        }
        return query.getList();
    }

    public CharacterSetEncoding getCharacterSetEncodingReference() {
        return characterSetEncodingReference;
    }

    public void setCharacterSetEncodingReference(CharacterSetEncoding characterSetEncodingReference) {
        this.characterSetEncodingReference = characterSetEncodingReference;
    }

    public String getCountryOID() {
        return countryOID;
    }

    public void setCountryOID(String countryOID) {
        this.countryOID = countryOID;
    }

    public List<CharacterSetEncoding> getCharacterSetEncoding() {
        return characterSetEncodingList;
    }

    public void setCharacterSetEncoding(List<CharacterSetEncoding> characterSetEncoding) {
        this.characterSetEncodingList = characterSetEncoding;
    }

    public Boolean getGenerateReligion() {
        return generateReligion;
    }

    public void setGenerateReligion(Boolean generateReligion) {
        this.generateReligion = generateReligion;
    }

    public Boolean getGenerateRace() {
        return generateRace;
    }

    public void setGenerateRace(Boolean generateRace) {
        this.generateRace = generateRace;
    }

    public Integer getMinNumberOfPatientMiddleName() {
        return minNumberOfPatientMiddleName;
    }

    public void setMinNumberOfPatientMiddleName(Integer minNumberOfPatientMiddleName) {
        this.minNumberOfPatientMiddleName = minNumberOfPatientMiddleName;
    }

    public Integer getMaxNumberOfPatientMiddleName() {
        return maxNumberOfPatientMiddleName;
    }

    public void setMaxNumberOfPatientMiddleName(Integer maxNumberOfPatientMiddleName) {
        this.maxNumberOfPatientMiddleName = maxNumberOfPatientMiddleName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIso() {
        return this.iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @FilterLabel
    public String getPrintableName() {
        return this.printableName;
    }

    public void setPrintableName(String printableName) {
        this.printableName = printableName;
    }

    public String getIso3() {
        return this.iso3;
    }

    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    public Integer getNumcode() {
        return this.numcode;
    }

    public void setNumcode(Integer numcode) {
        this.numcode = numcode;
    }

    public Boolean getEc() {
        return this.ec;
    }

    public void setEc(Boolean ec) {
        this.ec = ec;
    }

    public String getFlagUrl() {
        return flagUrl;
    }

    public void setFlagUrl(String flagUrl) {
        this.flagUrl = flagUrl;
    }

    public List<Language> getLanguage() {
        return language;
    }

    public void setLanguage(List<Language> language) {
        this.language = language;
    }

    public Boolean getIsLastNameSexed() {
        return isLastNameSexed;
    }

    public void setIsLastNameSexed(Boolean isLastNameSexed) {
        this.isLastNameSexed = isLastNameSexed;
    }

    public String getCx_4_code() {
        return cx_4_code;
    }

    public void setCx_4_code(String cx_4_code) {
        this.cx_4_code = cx_4_code;
    }

    public String getInseeCode() {
        return inseeCode;
    }

    public void setInseeCode(String inseeCode) {
        this.inseeCode = inseeCode;
    }

    public String toString() {
        return "CountryCode [ec=" + ec + ", flagUrl=" + flagUrl + ", id=" + id + ", iso=" + iso + ", iso3=" + iso3
                + ", name=" + name + ", numcode=" + numcode + ", printableName=" + printableName + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((iso == null) ? 0 : iso.hashCode());
        result = prime * result + ((iso3 == null) ? 0 : iso3.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((printableName == null) ? 0 : printableName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CountryCode other = (CountryCode) obj;
        if (iso == null) {
            if (other.iso != null) {
                return false;
            }
        } else if (!iso.equals(other.iso)) {
            return false;
        }
        if (iso3 == null) {
            if (other.iso3 != null) {
                return false;
            }
        } else if (!iso3.equals(other.iso3)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (printableName == null) {
            if (other.printableName != null) {
                return false;
            }
        } else if (!printableName.equals(other.printableName)) {
            return false;
        }
        return true;
    }

    public Boolean getUsable() {
        return isUsable;
    }

    public void setUsable(Boolean usable) {
        isUsable = usable;
    }

    // /////////////

}
