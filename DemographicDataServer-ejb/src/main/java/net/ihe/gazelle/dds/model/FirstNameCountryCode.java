/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import net.ihe.gazelle.common.profiling.MeasureCalls;
import net.ihe.gazelle.dds.utils.FirstNameCountryCodeQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>FirstNameCountryCode<br><br>
 * This class describes a FirstNameCountryCode object. It corresponds to the Code Country of a FirstName for a patient generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * FirstNameCountryCode object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the FirstNameCountryCode</li>
 * <li><b>firstNameSex</b> : Object corresponding the firstName and the sex for a person</li>
 * <li><b>iso</b> : Object corresponding to the country code for a person</li>
 * </ul></br>
 * <b>Example of FirstNameCountryCode</b> : FirstNameCountryCode(id, firstNameSex, iso) = (1, 5, 9) <br>
 *
 * @author Abderrazek boufahja - Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 * @class FirstNameCountryCode.java
 * @package net.ihe.gazelle.demographic.model
 * @see > sayed@irisa.fr - jchatel@irisa.fr  -  http://www.ihe-europe.org
 */

@MeasureCalls
@Entity
@Name("firstNameCountryCode")
@Table(name = "dds_first_name_country_code", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_first_name_country_code_sequence", sequenceName = "dds_first_name_country_code_id_seq", allocationSize = 1)

public class FirstNameCountryCode extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450904331536291760L;

    /**
     * Logger
     */
    @Logger
    private static Log log;

    //	Attributes (existing in database as a column)

    /**
     * Id of this FirstNameCountryCode object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_first_name_country_code_sequence")
    private Integer id;

    //	 Variables used for Foreign Keys : dds_patient_address table

    /**
     * The FirstNameSex object corresponding to this firstNameCountryCode
     */
    @ManyToOne
    @JoinColumn(name = "first_name_sex_id")
    private FirstNameSex firstNameSex;

    /**
     * The Iso3166CountryCode object corresponding to this firstNameCountryCode
     */
    @ManyToOne
    @JoinColumn(name = "country_code_id")
    private CountryCode countryCode;

    /**
     * The frequency of the firstName
     */
    @Column(name = "frequency")
    private Integer frequency;

    /**
     * The  accumulation for the frequency of the FirstNameCountryCode
     */
    @Column(name = "cumul")
    private Integer cumul;

    //	Constructors

    public FirstNameCountryCode() {

    }

    public FirstNameCountryCode(FirstNameSex firstNameSex, CountryCode countryCode) {
        this.firstNameSex = firstNameSex;
        this.countryCode = countryCode;
    }

    public FirstNameCountryCode(FirstNameCountryCode firstNameCountryCode) {
        this.firstNameSex = new FirstNameSex(firstNameCountryCode.getFirstNameSex());
        this.countryCode = new CountryCode(firstNameCountryCode.getCountryCode());
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public FirstNameSex getFirstNameSex() {
        return firstNameSex;
    }

    public void setFirstNameSex(FirstNameSex firstNameSex) {
        this.firstNameSex = firstNameSex;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getCumul() {
        return cumul;
    }

    public void setCumul(Integer cumul) {
        this.cumul = cumul;
    }


    public Integer getId() {
        return id;
    }

    // equals and hashCode //////////////////////////////////////////

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((countryCode == null) ? 0 : countryCode.hashCode());
        result = prime * result + ((cumul == null) ? 0 : cumul.hashCode());
        result = prime * result
                + ((firstNameSex == null) ? 0 : firstNameSex.hashCode());
        result = prime * result
                + ((frequency == null) ? 0 : frequency.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FirstNameCountryCode other = (FirstNameCountryCode) obj;
        if (countryCode == null) {
            if (other.countryCode != null) {
                return false;
            }
        } else if (!countryCode.equals(other.countryCode)) {
            return false;
        }
        if (cumul == null) {
            if (other.cumul != null) {
                return false;
            }
        } else if (!cumul.equals(other.cumul)) {
            return false;
        }
        if (firstNameSex == null) {
            if (other.firstNameSex != null) {
                return false;
            }
        } else if (!firstNameSex.equals(other.firstNameSex)) {
            return false;
        }
        if (frequency == null) {
            if (other.frequency != null) {
                return false;
            }
        } else if (!frequency.equals(other.frequency)) {
            return false;
        }
        return true;
    }

    // Methods  /////////////////////////////////////////////////////

    public static FirstNameCountryCode generateRandomFirstNameCountryCode(
            String iso,
            Gender genderOption,
            String firstNameNearby,
            String firstNameLike) {
        return FirstNameCountryCode.getBestMatchingFNCC(iso, genderOption, firstNameNearby, firstNameLike);
    }

    private static FirstNameCountryCode getBestMatchingFNCC(
            String iso,
            Gender genderOption,
            String firstNameNearby,
            String firstNameLike) {
        int maxCumul;
        FirstNameCountryCodeQueryBuilder query = new FirstNameCountryCodeQueryBuilder();
        if (iso != null) {
            query.addEq("countryCode.iso", iso);
        }
        if (genderOption != null) {
            query.addEq("firstNameSex.gender.description", genderOption.getDescription());
        }
        if (firstNameLike != null && !firstNameLike.isEmpty()) {
            query.addRestriction(HQLRestrictions.like("firstNameSex.firstName", firstNameLike, HQLRestrictionLikeMatchMode.ANYWHERE));
        }

        Integer cumulFirstNameCountryCode = query.getMax("cumul");
        if (cumulFirstNameCountryCode != null && cumulFirstNameCountryCode > 0) {
            int res = Patient.generateRandomInteger(1, cumulFirstNameCountryCode);
            query.addRestriction(HQLRestrictions.ge("cumul", res));
            cumulFirstNameCountryCode = query.getMin("cumul");
            query.addEq("cumul", cumulFirstNameCountryCode);
        }
        List<FirstNameCountryCode> listFNCCMatching = query.getList();
        if (listFNCCMatching.size() > 0) {
            maxCumul = FirstNameCountryCode.updateCumulOfListFNCC(listFNCCMatching);
            return FirstNameCountryCode.getBestFNCCByCumul(listFNCCMatching, maxCumul);
        } else {
            return null;
        }
    }

    private static int updateCumulOfListFNCC(List<FirstNameCountryCode> listFNCC) {
        int cumulation = 0;
        for (FirstNameCountryCode FNCC : listFNCC) {
            cumulation = cumulation + FNCC.getFrequency().intValue();
            FNCC.setCumul(cumulation);
        }
        return cumulation;
    }

    private static FirstNameCountryCode getBestFNCCByCumul(List<FirstNameCountryCode> listFNCC, int maxCumul) {
        FirstNameCountryCode firstNameCountryCode = new FirstNameCountryCode();
        if (listFNCC.size() > 0) {
            int index_FNCC = FirstNameCountryCode.getBestIndexByCumul(listFNCC, maxCumul);
            log.info("index_FNCC = " + index_FNCC);
            firstNameCountryCode = listFNCC.get(index_FNCC);
        }
        return firstNameCountryCode;
    }

    private static int getBestIndexByCumul(List<FirstNameCountryCode> list_FNCC, int maxCumul) {
        int minCumul = FirstNameCountryCode.getMinCumul(maxCumul);
        return FirstNameCountryCode.getNearestIndex(list_FNCC, maxCumul, minCumul);
    }

    private static int getMinCumul(int maxCumul) {
        int res = 0;
        res = (int) ((Math.random()) * maxCumul);
        log.info("mincumul = " + res);
        return res;
    }

    @Deprecated
    private static int getMaxCumul(List<FirstNameCountryCode> listFNCC) {
        int maxCumul = 0;
        for (FirstNameCountryCode FNCC : listFNCC) {
            if (FNCC.getCumul() > maxCumul) {
                maxCumul = FNCC.getCumul();
            }
        }
        return maxCumul;
    }

    private static int getNearestIndex(List<FirstNameCountryCode> listFNCC, int maxCumul, int minCumul) {
        int count = maxCumul;
        int res = 0;
        for (FirstNameCountryCode FNCC : listFNCC) {
            if ((FNCC.getCumul() <= count) && (FNCC.getCumul() >= minCumul)) {
                res = listFNCC.indexOf(FNCC);
                count = FNCC.getCumul();
            }
        }
        log.info("getNearestIndex = " + res);
        return res;
    }

}


