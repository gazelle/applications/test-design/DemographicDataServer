/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>LastName<br><br>
 * This class describes a LastName object. It corresponds to a LastName of the Patient generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * LastName object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the LastName</li>
 * <li><b>value</b> : Object corresponding to value of lastName</li>
 * </ul>
 * <b>Example of LastName</b> : LastName(id, value) = (1, "Meyer")<br>
 *
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */

@Entity
@Name("lastName")
@Table(name = "dds_last_name", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"gender_id", "value"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_last_name_sequence", sequenceName = "dds_last_name_id_seq", allocationSize = 1)
public class LastName extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450913312341293760L;


    //	Attributes (existing in database as a column)

    /**
     * Id of this LastName object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_last_name_sequence")

    private Integer id;


    /**
     * The value of the lastName (eg. "Eric")
     */
    @Column(name = "value", unique = true)
    private String value;


    /**
     * Gender object corresponding to this Lastname
     */
    @ManyToOne
    @JoinColumn(name = "gender_id")
    private Gender gender;


    public LastName() {

    }

    public LastName(String value) {
        this.gender = Gender.getGender_Mixed();
        this.value = value;
    }

    public LastName(String value, Gender gender) {
        this.gender = gender;
        this.value = value;
    }

    public LastName(LastName lastName) {
        if (lastName.value != null) {
            this.value = lastName.getValue();
        }
        if (lastName.getGender() != null) {
            this.gender = lastName.getGender();
        }

    }

    public static List<LastName> getLastNameList() {
        LastNameQuery query = new LastNameQuery();
        return query.getList();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gender == null) ? 0 : gender.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LastName other = (LastName) obj;
        if (gender == null) {
            if (other.gender != null) {
                return false;
            }
        } else if (!gender.equals(other.gender)) {
            return false;
        }
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }


}
