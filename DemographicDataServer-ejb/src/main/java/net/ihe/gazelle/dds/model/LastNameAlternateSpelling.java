/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>NameAlternateSpelling<br><br>
 * This class describes a NameAlternateSpelling object. It corresponds to a NameAlternateSpelling the lastName for a Patient generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * NameAlternateSpelling object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the LastNameAlternateSpelling</li>
 * <li><b>value</b> : Object corresponding to the value of a lastNameAlternateSpelling</li>
 * <li><b>lastName</b> : Object corresponding to the lastName of a nameAlternateSpelling</li>
 * </ul>
 * <b>Example of LastNameAlternateSpelling</b> : LastNameAlternateSpelling(id, value, lastName) = (1, "Erik", "Eric") <br>
 *
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */


@Entity
@Name("lastNameAlternateSpelling")
@Table(name = "dds_last_name_alternate_spelling", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_last_name_alternate_spelling_sequence", sequenceName = "dds_last_name_alternate_spelling_id_seq", allocationSize = 1)
public class LastNameAlternateSpelling extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450913331967893760L;


    //	Attributes (existing in database as a column)

    /**
     * Id of this LastNameAlternateSpelling object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_last_name_alternate_spelling_sequence")
    private Integer id;

    /**
     * The value of the lastNameAlternateSpelling
     */
    @Column(name = "value")
    private String value;

    //	 Variables used for Foreign Keys : dds_patient_address table

    /**
     * The LastName object corresponding to this LastNameAlternateSpelling
     */
    @ManyToOne
    @JoinColumn(name = "last_name_id")
    private LastName lastName;

    //	Constructors

    public LastNameAlternateSpelling() {

    }

    public LastNameAlternateSpelling(String value, LastName lastName) {
        this.value = value;
        this.lastName = lastName;

    }

    public LastNameAlternateSpelling(LastNameAlternateSpelling lastNameAlternateSpelling) {
        if (lastNameAlternateSpelling.getLastName() != null) {
            this.value = lastNameAlternateSpelling.getValue();
        }
        if (lastNameAlternateSpelling.getValue() != null) {
            this.lastName = new LastName(lastNameAlternateSpelling.getLastName());
        }
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<LastNameAlternateSpelling> getLastNameAlternateSpellingFiltered(LastName inLastName) {
        LastNameAlternateSpellingQuery query = new LastNameAlternateSpellingQuery();
        if (inLastName != null) {
            query.lastName().eq(inLastName);
        }
        return query.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LastName getLastName() {
        return lastName;
    }

    public void setLastName(LastName lastName) {
        this.lastName = lastName;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    // methods /////////////////////////////////////////////////////////

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LastNameAlternateSpelling other = (LastNameAlternateSpelling) obj;
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }


}
