package net.ihe.gazelle.dds.model;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;


@Entity
@Name("otherName")
@Table(name = "dds_other_name", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_other_name_sequence", sequenceName = "dds_other_name_id_seq", allocationSize = 1)

public class OtherName extends AuditModule implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -451239991541567890L;

    /**
     * Id of this dds_other_name object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_other_name_sequence")
    private Integer id;

    @Column(name = "name_type")
    private String nameType;

    @ManyToOne
    @JoinColumn(name = "first_name_sex_id")
    private FirstNameSex firstNameSex;

    @ManyToOne
    @JoinColumn(name = "last_name_id")
    private LastName lastName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public FirstNameSex getFirstNameSex() {
        return firstNameSex;
    }

    public void setFirstNameSex(FirstNameSex firstNameSex) {
        this.firstNameSex = firstNameSex;
    }

    public LastName getLastName() {
        return lastName;
    }

    public void setLastName(LastName lastName) {
        this.lastName = lastName;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((nameType == null) ? 0 : nameType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OtherName other = (OtherName) obj;
        if (nameType == null) {
            if (other.nameType != null) {
                return false;
            }
        } else if (!nameType.equals(other.nameType)) {
            return false;
        }
        return true;
    }


    public OtherName() {
        super();
    }


    public OtherName(String nameType, FirstNameSex firstNameSex,
                     LastName lastName) {
        super();
        this.nameType = nameType;
        this.firstNameSex = firstNameSex;
        this.lastName = lastName;
    }


    public static OtherName saveOtherName(OtherName otherName) {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        if (otherName != null) {
            OtherName on = em.merge(otherName);
            em.flush();
            return on;
        } else {
            return null;
        }
    }


}
