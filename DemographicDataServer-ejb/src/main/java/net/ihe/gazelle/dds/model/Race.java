/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import net.ihe.gazelle.hql.FilterLabel;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>Race<br><br>
 * This class describes a Race object. It corresponds to a Race generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * Race object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Race</li>
 * <li><b>code</b> : the code corresponding to the Race</li>
 * <li><b>description</b> : Object corresponding to a Race</li>
 * </ul>
 * <b>Example of Race</b> : Race(id, code, description) = (1, 1002-5, "Asiatic") <br>
 *
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */


@Entity
@Name("race")
@Table(name = "dds_race", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_race_sequence", sequenceName = "dds_race_id_seq", allocationSize = 1)
public class Race extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -761234331541291760L;

    //	Attributes (existing in database as a column)

    /**
     * Id of this Race object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_race_sequence")
    private Integer id;

    /**
     * The code of this Race
     */
    @Column(name = "code")
    private String code;

    /**
     * The race of this person
     */
    @Column(name = "description")
    private String description;

    //	Constructors

    public Race() {

    }

    public Race(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public Race(String description) {
        this.description = description;
    }

    public Race(Race race) {
        if (race.getCode() != null) {
            this.code = race.getCode();
        }
        if (race.getDescription() != null) {
            this.description = race.getDescription();
        }
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<Race> getRaceList() {
        RaceQuery query = new RaceQuery();
        return query.getList();
    }

    public static Race getRaceByDescription(String description) {
        RaceQuery query = new RaceQuery();
        query.description().eq(description);
        return query.getUniqueResult();
    }

    public static Race getRandomRace() {
        List<Race> raceList = Race.getRaceList();
        // get Random Race from the list
        if (raceList != null) {
            int res = Patient.generateRandomInteger(0, raceList.size() - 1);
            return raceList.get(res);
        }
        return null;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @FilterLabel
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Race other = (Race) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        } else if (!code.equals(other.code)) {
            return false;
        }
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        return true;
    }

}
