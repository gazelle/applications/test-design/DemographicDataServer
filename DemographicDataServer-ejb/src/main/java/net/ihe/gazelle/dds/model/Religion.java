/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import net.ihe.gazelle.hql.FilterLabel;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>Religion<br><br>
 * This class describes a Religion object. It corresponds to a Religion generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * Religion object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Religion</li>
 * <li><b>code</b> : Object corresponding to a code for the religion</li>
 * <li><b>description</b> : Object corresponding to a description for the religion</li>
 * </ul></br>
 * <b>Example of Religion</b> : Religion(id, code, description) = (1, "AME", "Christian") <br>
 *
 * @class Religion.java
 * @package net.ihe.gazelle.demographic.model
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @see            > sayed@irisa.fr - jchatel@irisa.fr  -  http://www.ihe-europe.org
 * @version 1.0 - 2009, May 6th
 */

@Entity
@Name("religion")
@Table(name = "dds_religion", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_religion_sequence", sequenceName = "dds_religion_id_seq", allocationSize = 1)

public class Religion extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -476534331541291760L;

    //	Attributes (existing in database as a column)

    /**
     * Id of this Religion object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_religion_sequence")
    private Integer id;

    /**
     * The religion of this person
     */
    @Column(name = "code")
    private String code;

    /**
     * The description of this Religion
     */
    @Column(name = "description")
    private String description;

    //	Constructors

    public Religion() {

    }

    public Religion(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public Religion(Religion religion) {
        if (religion.getDescription() != null && religion.getCode() != null) {

            this.description = religion.getDescription();
            this.code = religion.getCode();
        }
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************


    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @FilterLabel
    public String getDescription() {
        return description;
    }


    public static List<Religion> getReligionList() {
        ReligionQuery query = new ReligionQuery();
        query.description().order(true);
        return query.getList();
    }


    public static Religion getReligionByDescription(String description) {
        ReligionQuery query = new ReligionQuery();
        query.description().eq(description);
        return query.getUniqueResult();
    }


    public static Religion getRandomReligion() {

        // get Random Race from the list

        List<Religion> results = Religion.getReligionList();
        if (results != null) {
            int resultIndex = Patient.generateRandomInteger(0, results.size() - 1);
            return results.get(resultIndex);
        }
        return null;


    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Religion other = (Religion) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        } else if (!code.equals(other.code)) {
            return false;
        }
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        return true;
    }

}
