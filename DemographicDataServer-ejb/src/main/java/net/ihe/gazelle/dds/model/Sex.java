/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import net.ihe.gazelle.hql.FilterLabel;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>Sex<br><br>
 * This class describes a Sex object. It corresponds to a Sex generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * Sex object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Sex</li>
 * <li><b>code</b> : Object code corresponding to a sex</li>
 * <li><b>description</b> : Object corresponding to the displayName of the sex</li>
 * </ul></br>
 * <b>Example of Patient</b> : Sex(id, code, description) = (1, M, "Male") <br>
 *
 * @class Sex.java
 * @package net.ihe.gazelle.demographic.model
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @see            > sayed@irisa.fr - jchatel@irisa.fr  -  http://www.ihe-europe.org
 * @version 1.0 - 2009, May 6th
 */


@Entity
@Name("sex")
@Table(name = "dds_sex", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_sex_sequence", sequenceName = "dds_sex_id_seq", allocationSize = 1)
public class Sex extends AuditModule implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -451234511541291760L;


    private final static String SEX_MALE_STRING = "Male";
    private final static String SEX_FEMALE_STRING = "Female";
    private final static String SEX_OTHER_STRING = "Other";

    //	Attributes (existing in database as a column)

    /**
     * Id of this Sex object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_sex_sequence")
    private Integer id;

    /**
     * The Sex of this person
     */
    @Column(name = "code")
    private String code;

    /**
     * The description of the sex for this person
     */
    @Column(name = "description")
    private String description;

    //	Constructors

    public Sex() {

    }

    public Sex(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public Sex(Sex sex) {
        this.code = (sex.getCode());
        this.description = sex.getDescription();
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @FilterLabel
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Sex getSEX_MALE() {
        return Sex.getSexByDescription(SEX_MALE_STRING);
    }

    public static Sex getSEX_FEMALE() {
        return Sex.getSexByDescription(SEX_FEMALE_STRING);
    }

    public static Sex getSEX_OTHER() {
        return Sex.getSexByDescription(SEX_OTHER_STRING);
    }

    public static Sex getSexByDescription(String sex) {
        SexQuery query = new SexQuery();
        query.description().eq(sex);
        return query.getUniqueResult();
    }

    public static int getLengthOfTable() {
        SexQuery query = new SexQuery();
        return query.getCount();
    }

    /**
     * This methods returns a list containing all the FirstNameSex object existing in the database
     *
     * @return List <Sex> : list of all Sex objects
     */
    public static List<Sex> getAllSex() {
        SexQuery query = new SexQuery();
        return query.getList();
    }

    public static Sex getRandomSex() {

        List<Sex> results = Sex.getAllSex();
        if (results != null) {
            int resultIndex = Patient.generateRandomInteger(0, results.size() - 1);
            return results.get(resultIndex);
        }
        return null;

    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Sex other = (Sex) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        } else if (!code.equals(other.code)) {
            return false;
        }
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        return true;
    }


}
