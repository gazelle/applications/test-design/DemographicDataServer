/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;

/**
 * <b>Class Description :  </b>Street<br><br>
 * This class describes a Street object. It corresponds to a Street generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * Street object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Street</li>
 * <li><b>value</b> : Object corresponding to a Street</li>
 * </ul></br>
 * <b>Example of Street</b> : Street(id, value) = (6, "Charles de Gaulle") <br>
 *
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 * @class Street.java
 * @package net.ihe.gazelle.demographic.model
 * @see > sayed@irisa.fr - jchatel@irisa.fr  -  http://www.ihe-europe.org
 */

@Entity
@Name("street")
@Table(name = "dds_street", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_street_sequence", sequenceName = "dds_street_id_seq", allocationSize = 1)

public class Street extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -451219281541291760L;

    //	Attributes (existing in database as a column)

    /**
     * Id of this Street object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_street_sequence")
    private Integer id;

    /**
     * The name of the street of the address
     */
    @Column(name = "value")
    private String value;

    //	Constructors

    public Street() {

    }

    public Street(String value) {
        this.value = value;
    }

    public Street(Street street) {
        if (street.getValue() != null) {
            this.value = street.getValue();
        }
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Street other = (Street) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

}
