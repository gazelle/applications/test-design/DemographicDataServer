/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.model;


import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;


/**
 * <b>Class Description :  </b>Iso3166CountryCode<br><br>
 * This class describes the user ip address when generate Patient addresses.
 * <p>
 * UserIpAddress possesses the following attributes :
 * <ul>
 * <li><b>id</b> : UserIpAddress id</li>
 * <li><b>ipAddress</b> : Ip address of the user</li>
 * <li><b>requestTime</b> : Time of the request</li>
 * <li><b>nominatimRequestNumber : Number of request with Nominatim.</li>
 * <li><b>googleRequestNumber : Number of request with google.</li>
 * <li><b>geonameRequestNumber : Number of request with geoname.</li>
 * </ul></br>
 * <b>Example of UserIpAddress</b> : (11,'127.0.0.1', '10:22:11',2,3,0); <br>
 *
 * @author Nicolas Lefebvre / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, october 22th
 * @class UserIpAddress.java
 * @package net.ihe.gazelle.users
 * @see >        nicolas.lefebvre@inria.fr -  http://www.ihe-europe.org
 */

@Entity
@Name("UserIpAddress")
@Table(name = "dds_user_ip_address", schema = "public")
@SequenceGenerator(name = "dds_user_ip_address_sequence", sequenceName = "dds_user_ip_address_id_seq", allocationSize = 1)
public class UserIpAddress implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450911771541256760L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_user_ip_address_sequence")
    private Integer id;

    @Column(name = "ip_address", nullable = false)
    @NotNull
    private String ipAddress;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "request_time", nullable = false)
    @NotNull
    private Date requestTime;

    @Column(name = "nominatim_request_number", nullable = false)
    @NotNull
    private Integer nominatimRequestNumber;

    @Column(name = "google_request_number", nullable = false)
    @NotNull
    private Integer googleRequestNumber;

    @Column(name = "geoname_request_number", nullable = false)
    @NotNull
    private Integer geonameRequestNumber;


    //Constructors
    public UserIpAddress() {

    }


    public UserIpAddress(Integer id, String ipAddress, Date requestTime,
                         Integer nominatimRequestNumber, Integer googleRequestNumber,
                         Integer geonameRequestNumber) {
        super();
        this.id = id;
        this.ipAddress = ipAddress;
        this.requestTime = requestTime;
        this.nominatimRequestNumber = nominatimRequestNumber;
        this.googleRequestNumber = googleRequestNumber;
        this.geonameRequestNumber = geonameRequestNumber;
    }


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getIpAddress() {
        return ipAddress;
    }


    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }


    public Date getRequestTime() {
        return requestTime;
    }


    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }


    public Integer getNominatimRequestNumber() {
        return nominatimRequestNumber;
    }


    public void setNominatimRequestNumber(Integer nominatimRequestNumber) {
        this.nominatimRequestNumber = nominatimRequestNumber;
    }


    public Integer getGoogleRequestNumber() {
        return googleRequestNumber;
    }


    public void setGoogleRequestNumber(Integer googleRequestNumber) {
        this.googleRequestNumber = googleRequestNumber;
    }


    public Integer getGeonameRequestNumber() {
        return geonameRequestNumber;
    }


    public void setGeonameRequestNumber(Integer geonameRequestNumber) {
        this.geonameRequestNumber = geonameRequestNumber;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((geonameRequestNumber == null) ? 0 : geonameRequestNumber
                .hashCode());
        result = prime
                * result
                + ((googleRequestNumber == null) ? 0 : googleRequestNumber
                .hashCode());
        result = prime * result
                + ((ipAddress == null) ? 0 : ipAddress.hashCode());
        result = prime
                * result
                + ((nominatimRequestNumber == null) ? 0
                : nominatimRequestNumber.hashCode());
        result = prime * result
                + ((requestTime == null) ? 0 : requestTime.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserIpAddress other = (UserIpAddress) obj;
        if (geonameRequestNumber == null) {
            if (other.geonameRequestNumber != null) {
                return false;
            }
        } else if (!geonameRequestNumber.equals(other.geonameRequestNumber)) {
            return false;
        }
        if (googleRequestNumber == null) {
            if (other.googleRequestNumber != null) {
                return false;
            }
        } else if (!googleRequestNumber.equals(other.googleRequestNumber)) {
            return false;
        }
        if (ipAddress == null) {
            if (other.ipAddress != null) {
                return false;
            }
        } else if (!ipAddress.equals(other.ipAddress)) {
            return false;
        }
        if (nominatimRequestNumber == null) {
            if (other.nominatimRequestNumber != null) {
                return false;
            }
        } else if (!nominatimRequestNumber.equals(other.nominatimRequestNumber)) {
            return false;
        }
        if (requestTime == null) {
            if (other.requestTime != null) {
                return false;
            }
        } else if (!requestTime.equals(other.requestTime)) {
            return false;
        }
        return true;
    }


    // methods //////////////////////////////////////////
    public static UserIpAddress mergeUserIpAddress(UserIpAddress user) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");

        if (user.getIpAddress() != null) {
            user = entityManager.merge(user);
            entityManager.flush();
        }

        return user;
    }


    public static void emptyTable(UserIpAddress user) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.remove(user);
    }

    public static List<UserIpAddress> getAllUsersRequests() {
        UserIpAddressQuery query = new UserIpAddressQuery();
        query.requestTime().order(true);
        return query.getList();
    }

    public static UserIpAddress findUserIpAddress(int userId) {
        UserIpAddressQuery query = new UserIpAddressQuery();
        query.id().eq(userId);
        return query.getUniqueResult();
    }


}
