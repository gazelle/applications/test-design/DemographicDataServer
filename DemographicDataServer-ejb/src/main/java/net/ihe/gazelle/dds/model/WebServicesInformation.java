package net.ihe.gazelle.dds.model;


import net.ihe.gazelle.dds.admin.ApplicationConfigurationProvider;
import net.ihe.gazelle.dds.utils.UserIpAddressQueryBuilder;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Copyright 2008 IHE International (http://www.ihe.net)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * <b>Class Description :  </b>Iso3166CountryCode<br><br>
 * This class describes the information about the Web Services that DDS uses.
 * <p>
 * WebServicesInformation possesses the following attributes :
 * <ul>
 * <li><b>id</b> : WebServicesInformation id</li>
 * <li><b>wsName</b> : Name of the Ws</li>
 * <li><b>requestMaximumNumber</b> : The maximum number of requests that the Ws allows.</li>
 * <li><b>requestSinceLastDay</b> : The number of Ws requests since the last 24 hours.</li>
 * <li><b>wsURL</b> : URL of the Ws</li>
 * <li><b>lastUserIpId</b> : Save the id of the last user Ip.</li>
 * </ul></br>
 * <b>Example of WebServicesInformation</b> : ('Nominatim', 'http://nominatim.openstreetmap.org/reverse?format=xml&lat=&lon=&lng=&zoom=18&addressdetails=1', '86400','7000'); <br>
 *
 * @author Nicolas Lefebvre / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, october 22th
 * @class WebServicesInformation.java
 * @package net.ihe.gazelle.users
 * @see >        nicolas.lefebvre@inria.fr -  http://www.ihe-europe.org
 */


@Entity
@Name("WebServicesInformation")
@Table(name = "dds_web_services_information", schema = "public")
@SequenceGenerator(name = "dds_web_services_information_sequence", sequenceName = "dds_web_services_information_id_seq", allocationSize = 1)
public class WebServicesInformation implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450977881541256760L;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_web_services_information_sequence")
    private Integer id;

    @Column(name = "ws_name", nullable = false)
    @NotNull
    private String wsName;

    @Column(name = "request_maximum_number", nullable = false)
    @NotNull
    private Integer requestMaximumNumber;

    @Column(name = "request_since_last_day", nullable = false)
    @NotNull
    private Integer requestSinceLastDay;

    @Column(name = "total_request", nullable = false)
    @NotNull
    private Integer totalRequest;

    @Column(name = "ws_url", nullable = false)
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$|^ftp://.+$)", message = "Please enter a valid URL (eg. http\\://www.ihe.net )")
    @NotNull
    private String wsURL;

    @Column(name = "last_user_ip_id", nullable = false)
    @NotNull
    private Integer lastUserIpId;

    @Column(name = "last_request_time", nullable = true)
    private Date lastRequestTime;

    @Column(name = "ws_priority", nullable = false)
    @NotNull
    private Integer wsPriority;


    //Constructor

    public WebServicesInformation() {

    }


    public WebServicesInformation(Integer id, String wsName,
                                  Integer requestMaximumNumber, Integer requestSinceLastDay,
                                  Integer totalRequest, String wsURL, Integer lastUserIpId,
                                  Date lastRequestTime,Integer wsPriority) {
        super();
        this.id = id;
        this.wsName = wsName;
        this.requestMaximumNumber = requestMaximumNumber;
        this.requestSinceLastDay = requestSinceLastDay;
        this.totalRequest = totalRequest;
        this.wsURL = wsURL;
        this.lastUserIpId = lastUserIpId;
        this.lastRequestTime = lastRequestTime;
        this.wsPriority = wsPriority;
    }

    // methods //////////////////////////////////////////
    public static WebServicesInformation mergeWebServicesInformation(WebServicesInformation ws) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");

        if (ws.getWsName() != null && ws.getRequestMaximumNumber() != null) {
            entityManager.merge(ws);
            entityManager.flush();
        }

        return ws;
    }

    public static List<WebServicesInformation> getAllWsInformation() {
        WebServicesInformationQuery query = new WebServicesInformationQuery();
        return query.getList();
    }

    public static WebServicesInformation getWsInformationWithName(String name) {
        WebServicesInformationQuery query = new WebServicesInformationQuery();
        query.wsName().eq(name);
        return query.getUniqueResult();
    }

    public static int getNumberOfRequestForDDS() {
        UserIpAddressQuery query = new UserIpAddressQuery();
        return query.getCount();
    }

    public static int getNumberOfRequestForNominatim() {
        UserIpAddressQueryBuilder query = new UserIpAddressQueryBuilder();
        return query.computeSumOnAttribute("nominatimRequestNumber");
    }

    public static int getNumberOfRequestForGoogle() {
        UserIpAddressQueryBuilder query = new UserIpAddressQueryBuilder();
        return query.computeSumOnAttribute("googleRequestNumber");
    }

    public static int getNumberOfRequestForGeoname() {
        UserIpAddressQueryBuilder query = new UserIpAddressQueryBuilder();
        return query.computeSumOnAttribute("geonameRequestNumber");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWsPriority() {
        return wsPriority;
    }

    public void setWsPriority(Integer wsPriority) {
        this.wsPriority = wsPriority;
    }


    public String getWsName() {
        return wsName;
    }

    public void setWsName(String wsName) {
        this.wsName = wsName;
    }

    public Integer getRequestMaximumNumber() {
        return requestMaximumNumber;
    }

    public void setRequestMaximumNumber(Integer requestMaximumNumber) {
        this.requestMaximumNumber = requestMaximumNumber;
    }

    public Integer getRequestSinceLastDay() {
        return requestSinceLastDay;
    }

    public void setRequestSinceLastDay(Integer requestSinceLastDay) {
        this.requestSinceLastDay = requestSinceLastDay;
    }

    public Integer getTotalRequest() {
        return totalRequest;
    }

    public void setTotalRequest(Integer totalRequest) {
        this.totalRequest = totalRequest;
    }

    public String getWsURL() {
        return wsURL;
    }

    public void setWsURL(String wsURL) {
        this.wsURL = wsURL;
    }

    public Integer getLastUserIpId() {
        return lastUserIpId;
    }

    public void setLastUserIpId(Integer lastUserIpId) {
        this.lastUserIpId = lastUserIpId;
    }

    public Date getLastRequestTime() {
        return lastRequestTime;
    }

    public void setLastRequestTime(Date lastRequestTime) {
        this.lastRequestTime = lastRequestTime;
    }


    public String getPriorityService(){
        String namePriorityService = "";
        WebServicesInformation webServicesInformation = new WebServicesInformation();
        List<WebServicesInformation> listWs = webServicesInformation.getAllWsInformation();

        for(WebServicesInformation wsInfo: listWs) {
            if (wsInfo.getWsPriority().equals(1)) {
                if (isRequestAccess(wsInfo.getRequestSinceLastDay(), wsInfo.getRequestMaximumNumber())) {
                    namePriorityService = wsInfo.getWsName();
                    break;
                }else{
                        for(WebServicesInformation wsInfo2: listWs) {
                            if (wsInfo2.getWsPriority().equals(2)) {
                                if (isRequestAccess(wsInfo2.getRequestSinceLastDay(), wsInfo2.getRequestMaximumNumber())) {
                                    namePriorityService = wsInfo2.getWsName();
                                    break;
                                }
                            }
                        }
                }
            }
        }
        return namePriorityService;
    }


    public Boolean isRequestAccess(Integer requestSinceLastDay, Integer requestMaximumNumber){
        boolean isAccess = false;
        if(requestSinceLastDay < requestMaximumNumber){
            isAccess = true;
        }
        return isAccess;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((lastRequestTime == null) ? 0 : lastRequestTime.hashCode());
        result = prime * result
                + ((lastUserIpId == null) ? 0 : lastUserIpId.hashCode());
        result = prime
                * result
                + ((requestMaximumNumber == null) ? 0 : requestMaximumNumber
                .hashCode());
        result = prime
                * result
                + ((requestSinceLastDay == null) ? 0 : requestSinceLastDay
                .hashCode());
        result = prime * result
                + ((totalRequest == null) ? 0 : totalRequest.hashCode());
        result = prime * result + ((wsName == null) ? 0 : wsName.hashCode());
        result = prime * result + ((wsURL == null) ? 0 : wsURL.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        WebServicesInformation other = (WebServicesInformation) obj;
        if (lastRequestTime == null) {
            if (other.lastRequestTime != null) {
                return false;
            }
        } else if (!lastRequestTime.equals(other.lastRequestTime)) {
            return false;
        }
        if (lastUserIpId == null) {
            if (other.lastUserIpId != null) {
                return false;
            }
        } else if (!lastUserIpId.equals(other.lastUserIpId)) {
            return false;
        }
        if (requestMaximumNumber == null) {
            if (other.requestMaximumNumber != null) {
                return false;
            }
        } else if (!requestMaximumNumber.equals(other.requestMaximumNumber)) {
            return false;
        }
        if (requestSinceLastDay == null) {
            if (other.requestSinceLastDay != null) {
                return false;
            }
        } else if (!requestSinceLastDay.equals(other.requestSinceLastDay)) {
            return false;
        }
        if (totalRequest == null) {
            if (other.totalRequest != null) {
                return false;
            }
        } else if (!totalRequest.equals(other.totalRequest)) {
            return false;
        }
        if (wsName == null) {
            if (other.wsName != null) {
                return false;
            }
        } else if (!wsName.equals(other.wsName)) {
            return false;
        }
        if (wsURL == null) {
            if (other.wsURL != null) {
                return false;
            }
        } else if (!wsURL.equals(other.wsURL)) {
            return false;
        }
        return true;
    }


}



