package net.ihe.gazelle.dds.model;

/**
 * Created by gthomazon on 23/06/16.
 */

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.persistence.EntityManager;
import java.io.Serializable;

@Name("webServicesInformationManager")
@Scope(ScopeType.PAGE)
@GenerateInterface("WebServicesInformationManagerLocal")
public class WebServicesInformationManager implements WebServicesInformationManagerLocal, Serializable {

    private static final long serialVersionUID = -8827946856533110546L;

    private WebServicesInformation selectedWS;
    private WebServicesInformation webService = null;

    public WebServicesInformation getSelectedWS() {
        return selectedWS;
    }

    public void setSelectedWS(WebServicesInformation selectedWS) {
        this.selectedWS = selectedWS;
    }

    public WebServicesInformation getWebService() {
        return webService;
    }

    public void setWebService(WebServicesInformation webService) {
        this.webService = webService;
    }

    public void createWS() {
        setWebService(new WebServicesInformation());
        webService.setRequestSinceLastDay(0);
        webService.setTotalRequest(0);
        webService.setLastUserIpId(0);
    }

    public void addWebServicesInformation() {
        if (webService != null) {
            setSelectedWS(webService);
            mergeWebServicesInformation();
            FacesMessages.instance().clearGlobalMessages();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, selectedWS.getWsName() + " is added");
        }
    }

    public void mergeWebServicesInformation() {
        if (selectedWS != null) {
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");

            if (selectedWS.getWsName() != null && selectedWS.getRequestMaximumNumber() != null) {
                entityManager.merge(selectedWS);
                entityManager.flush();
            }
            FacesMessages.instance().add(StatusMessage.Severity.INFO, selectedWS.getWsName() + " is updated");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No WebService selected");
        }
    }

    public void deleteWebServicesInformation() {
        if (selectedWS != null) {
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            this.selectedWS = entityManager.find(WebServicesInformation.class, selectedWS.getId());
            if (selectedWS != null) {
                String wsName = selectedWS.getWsName();
                entityManager.remove(selectedWS);
                entityManager.flush();
                FacesMessages.instance().add(StatusMessage.Severity.INFO, wsName + " WebService is deleted");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No WebService selected");
        }
    }


}
