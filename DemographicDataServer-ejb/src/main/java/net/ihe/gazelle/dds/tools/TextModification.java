/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.tools;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * @author Abderrazek Boufahja / INRIA Rennes IHE development Project
 */
public class TextModification {

    private static final String BR = "<br/>";
    private static final String SPAN = "</span>";

    public TextModification() {
    }

    public static String formatString(String fileString) {
        StringBuffer result = new StringBuffer();
        if (fileString != null) {
            String[] listLign = fileString.split("\n");
            for (String s : listLign) {
                s = s.replaceAll("\t", "----");
                s = StringEscapeUtils.escapeHtml(s);
                result.append(s + BR);
            }
        }
        return result.toString();
    }

    public static String formatColoredString(String fileString) {
        StringBuffer result = new StringBuffer();
        if (fileString != null) {
            String[] listLign = fileString.split("\n");
            for (String s : listLign) {
                s = s.replaceAll("\t", "----");
                s = StringEscapeUtils.escapeHtml(s);
                if ((s.indexOf("Error")) != (-1)) {
                    result.append("<span style=\"color:#aa0000;\">" + s + SPAN + BR);
                } else if ((s.indexOf("Warning")) != (-1)) {
                    result.append("<span style=\"color:#0000aa;\">" + s + SPAN + BR);
                } else {
                    result.append(s + BR);
                }
            }
        }
        return result.toString();
    }

    public static String formatBBCodeString(String fileString) {
        StringBuffer result = new StringBuffer();
        if (fileString != null) {
            String[] listLign = fileString.split("\n");
            for (String s : listLign) {
                s = s.replace("\t", ". . . ");
                s = s.replace("   ", ". . . ");
                s = s.replace("	", ". . . ");

                s = StringEscapeUtils.escapeHtml(s);

                s = s.replace("[b]", "<b>");
                s = s.replace("[/b]", "</b>");
                s = s.replace("[i]", "<i>");
                s = s.replace("[/i]", "</i>");
                s = s.replace("[red]", "<span style=\"color:#aa0000;\">");
                s = s.replace("[/red]", SPAN);
                s = s.replace("[blue]", "<span style=\"color:#0000aa;\">");
                s = s.replace("[/blue]", SPAN);
                s = s.replace("[yel]", "<span style=\"color:#DAA520;\">");
                s = s.replace("[/yel]", SPAN);

                result.append(s + BR);
            }
        }
        return result.toString();
    }

}
