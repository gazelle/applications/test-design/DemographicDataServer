package net.ihe.gazelle.dds.utils;

import net.ihe.gazelle.dds.model.UserIpAddress;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.beans.HQLOrder;
import net.ihe.gazelle.hql.beans.HQLRestrictionValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;
import javax.persistence.Query;

public class UserIpAddressQueryBuilder extends HQLQueryBuilder<UserIpAddress> {

    private static Logger log = LoggerFactory.getLogger(UserIpAddressQueryBuilder.class);

    public UserIpAddressQueryBuilder() {
        super(UserIpAddress.class);
    }

    public Integer computeSumOnAttribute(String pathToAttribute) {
        StringBuilder sb = new StringBuilder("select sum(" + pathToAttribute + ") ");
        // First build where, to get all paths in from
        StringBuilder sbWhere = new StringBuilder();
        HQLRestrictionValues values = buildQueryWhere(sbWhere);
        // Build order, to get all paths in from
        buildQueryOrder(sbWhere);

        // add order columns if needed, cannot sort without it!
        // will be removed at the end (result list will be Object[] then)
        for (HQLOrder order : getOrders()) {
            sb.append(", ").append(order.getPath());
        }
        sb.append(buildQueryFrom());
        sb.append(sbWhere);

        Query query = createRealQuery(sb);

        buildQueryWhereParameters(query, values);
        try {
            Long sum = (Long) query.getSingleResult();
            if (sum == null) {
                return 0;
            } else {
                return sum.intValue();
            }
        } catch (NoResultException e) {
            log.error("" + e.getMessage());
            return 0;
        }
    }

}
