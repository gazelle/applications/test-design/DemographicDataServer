package net.ihe.gazelle.dds.utils;

import net.ihe.gazelle.dds.model.ApplicationConfiguration;
import org.jboss.seam.core.ResourceBundle;

import javax.persistence.EntityManager;

public class Utils {


    public static String getMaritalStatusDescription(String value) {
        if (value.equals("M")) {
            return ResourceBundle.instance().getString("gazelle.dds.Married");
        } else if (value.equals("S")) {
            return ResourceBundle.instance().getString("gazelle.dds.Single");
        } else if (value.equals("D")) {
            return ResourceBundle.instance().getString("gazelle.dds.Divorced");
        } else {
            return value;
        }
    }


    public static String getNameTypeDescription(String value) {
        if (value.equals("A")) {
            return ResourceBundle.instance().getString("gazelle.dds.AliasName");
        } else if (value.equals("D")) {
            return ResourceBundle.instance().getString("gazelle.dds.DisplayName");
        } else if (value.equals("L")) {
            return ResourceBundle.instance().getString("gazelle.dds.LegalName");
        } else if (value.equals("M")) {
            return ResourceBundle.instance().getString("gazelle.dds.MaidenName");
        } else {
            return value;
        }
    }


    public static int getHL7v3TimeOut(EntityManager entityManager) {
        String timeoutString = ApplicationConfiguration.getValueOfVariable("hl7v3_sender_timeout");

        Integer timeout = Integer.valueOf(timeoutString);
        return timeout.intValue();
    }


}
