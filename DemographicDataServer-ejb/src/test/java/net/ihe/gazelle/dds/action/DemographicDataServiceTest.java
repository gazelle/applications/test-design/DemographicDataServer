package net.ihe.gazelle.dds.action;

import net.ihe.gazelle.dds.admin.ApplicationConfigurationProvider;
import net.ihe.gazelle.dds.admin.ApplicationConfigurationProviderLocal;
import net.ihe.gazelle.dds.model.ApplicationConfiguration;
import net.ihe.gazelle.dds.model.ApplicationConfigurationQuery;
import net.ihe.gazelle.dds.model.CountryCode;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.Component;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.xml.soap.SOAPException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EntityManagerService.class, ApplicationConfigurationProvider.class, AddressInfoBuilder.class, Component.class})
public class DemographicDataServiceTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    DemographicDataService demographicDataService = new DemographicDataService();

    static EntityManager entityManager;

    @BeforeClass
    public static void initializeDB() {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        addTestEntitiesToDB(entityManager);
        entityManager.getTransaction().commit();
    }

    @Before
    public void setup() {
        PowerMockito.mockStatic(EntityManagerService.class);
        Mockito.when(EntityManagerService.provideEntityManager()).thenReturn(entityManager);
        PowerMockito.mockStatic(ApplicationConfigurationProvider.class);
        PowerMockito.mockStatic(Component.class);
        Mockito.when(Component.getInstance("entityManager")).thenReturn(entityManager);
        ApplicationConfigurationProviderLocal providerLocal = new ApplicationConfigurationProvider();
        Mockito.when(ApplicationConfigurationProvider.instance()).thenReturn(providerLocal);
    }

    /**
     * Initialization method to create EntityManager and JPADAO test implementation.
     */
    @Before
    public void initializeEntityManager() {
        entityManager.getTransaction().begin();
    }

    /**
     * Close the transaction corresponding to the test case when it is over.
     */
    @After
    public void commitTransaction() {
        entityManager.getTransaction().commit();
    }

    @Test
    public void returnAddress() {

    }

    @Test
    public void returnCountryInseeCodeOK() throws SOAPException {
        String insee = demographicDataService.returnCountryInseeCode("AR");
        assertEquals("99415", insee);
    }

    @Test
    public void returnCountryInseeCodeUnknown() throws SOAPException {
        String insee = demographicDataService.returnCountryInseeCode("FR");
        assertNull(insee);
    }

    @Test(expected = SOAPException.class)
    public void returnCountryInseeCodeNull() throws SOAPException {
        demographicDataService.returnCountryInseeCode(null);
    }

    @Test(expected = SOAPException.class)
    public void returnCountryInseeCodeEmpty() throws SOAPException {
        demographicDataService.returnCountryInseeCode("");
    }

    @Test(expected = SOAPException.class)
    public void returnTownInseeCodeMinimal() throws SOAPException {
        setApplicationMode("minimal");
        demographicDataService.returnTownInseeCode("Thorigné-Fouillard");
    }

    @Test(expected = SOAPException.class)
    public void returnTownInseeCodeNull() throws SOAPException {
        setApplicationMode("full");
        demographicDataService.returnTownInseeCode(null);
    }

    @Test(expected = SOAPException.class)
    public void returnTownInseeCodeEmpty() throws SOAPException {
        demographicDataService.returnTownInseeCode("");
    }

    @Test
    public void returnTownInseeCodeOk() throws Exception {
        setApplicationMode("full");
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "<searchresults timestamp='Thu, 24 Jun 21 12:33:20 +0000' attribution='Data © OpenStreetMap contributors, ODbL 1.0. http://www.openstreetmap.org/copyright' querystring='Toulon, France' exclude_place_ids='256862719,257074068,44967380,48687727,26650672,16672517,46330815,51508122,76310288,71042684' more_url='https://nominatim.openstreetmap.org/search/?city=Toulon&amp;country=France&amp;extratags=1&amp;exclude_place_ids=256862719%2C257074068%2C44967380%2C48687727%2C26650672%2C16672517%2C46330815%2C51508122%2C76310288%2C71042684&amp;format=xml'>\n" +
                "<place place_id='256862719' osm_type='relation' osm_id='35280' place_rank='16' address_rank='16' boundingbox=\"43.1010435,43.171494,5.8795028,5.9873841\" lat='43.1257311' lon='5.9304919' display_name='Toulon, Var, Provence-Alpes-Côte d&#039;Azur, France métropolitaine, France' class='boundary' type='administrative' importance='0.82452584494329' icon='https://nominatim.openstreetmap.org/ui/mapicons//poi_boundary_administrative.p.20.png'>\n" +
                "<extratags><tag key=\"capital\" value=\"6\"/><tag key=\"wikidata\" value=\"Q44160\"/><tag key=\"ref:INSEE\" value=\"83137\"/><tag key=\"wikipedia\" value=\"fr:Toulon\"/><tag key=\"population\" value=\"171953\"/><tag key=\"linked_place\" value=\"city\"/><tag key=\"ref:FR:SIREN\" value=\"218301372\"/></extratags></place><place place_id='257074068' osm_type='relation' osm_id='1693227' place_rank='14' address_rank='14' boundingbox=\"42.9820137,43.3401768,5.6719923,6.5112861\" lat='43.18305925' lon='6.059109217575072' display_name='Toulon, Var, Provence-Alpes-Côte d&#039;Azur, France métropolitaine, France' class='boundary' type='administrative' importance='0.52500394596232' icon='https://nominatim.openstreetmap.org/ui/mapicons//poi_boundary_administrative.p.20.png'>\n" +
                "<extratags><tag key=\"wikidata\" value=\"Q537277\"/><tag key=\"ref:INSEE\" value=\"832\"/><tag key=\"wikipedia\" value=\"fr:Arrondissement de Toulon\"/></extratags></place><place place_id='44967380' osm_type='node' osm_id='3474742599' place_rank='19' address_rank='18' boundingbox=\"45.6653721,45.7053721,-0.9139,-0.8739\" lat='45.6853721' lon='-0.8939' display_name='Toulon, Sablonceaux, Saintes, Charente-Maritime, Nouvelle-Aquitaine, France métropolitaine, 17600, France' class='place' type='village' importance='0.495' icon='https://nominatim.openstreetmap.org/ui/mapicons//poi_place_village.p.20.png'>\n" +
                "<extratags></extratags></place><place place_id='48687727' osm_type='node' osm_id='3979416298' place_rank='20' address_rank='20' boundingbox=\"45.6648863,45.7048863,-0.9137986,-0.8737986\" lat='45.6848863' lon='-0.8937986' display_name='Toulon, Sablonceaux, Saintes, Charente-Maritime, Nouvelle-Aquitaine, France métropolitaine, 17600, France' class='place' type='hamlet' importance='0.47' icon='https://nominatim.openstreetmap.org/ui/mapicons//poi_place_village.p.20.png'>\n" +
                "<extratags></extratags></place><place place_id='26650672' osm_type='node' osm_id='2651603476' place_rank='20' address_rank='22' boundingbox=\"46.361644,46.381644,5.1111038,5.1311038\" lat='46.371644' lon='5.1211038' display_name='Toulon, Jayat, Bourg-en-Bresse, Ain, Auvergne-Rhône-Alpes, France métropolitaine, 01340, France' class='place' type='neighbourhood' importance='0.47'>\n" +
                "<extratags></extratags></place><place place_id='16672517' osm_type='node' osm_id='1701374239' place_rank='20' address_rank='20' boundingbox=\"47.851841,47.891841,-1.3385303,-1.2985303\" lat='47.871841' lon='-1.3185303' display_name='Toulon, Forges-la-Forêt, Fougères-Vitré, Ille-et-Vilaine, Bretagne, France métropolitaine, 35640, France' class='place' type='hamlet' importance='0.47' icon='https://nominatim.openstreetmap.org/ui/mapicons//poi_place_village.p.20.png'>\n" +
                "<extratags></extratags></place><place place_id='46330815' osm_type='node' osm_id='3670276001' place_rank='20' address_rank='20' boundingbox=\"48.5936669,48.6336669,-3.7999166,-3.7599166\" lat='48.6136669' lon='-3.7799166' display_name='Toulon, Garlan, Morlaix, Finistère, Bretagne, France métropolitaine, 29610, France' class='place' type='hamlet' importance='0.47' icon='https://nominatim.openstreetmap.org/ui/mapicons//poi_place_village.p.20.png'>\n" +
                "<extratags></extratags></place><place place_id='51508122' osm_type='node' osm_id='4471537542' place_rank='20' address_rank='20' boundingbox=\"45.6303919,45.6703919,1.525131,1.565131\" lat='45.6503919' lon='1.545131' display_name='Toulon, Saint-Méard, Limoges, Haute-Vienne, Nouvelle-Aquitaine, France métropolitaine, 87130, France' class='place' type='hamlet' importance='0.47' icon='https://nominatim.openstreetmap.org/ui/mapicons//poi_place_village.p.20.png'>\n" +
                "<extratags></extratags></place><place place_id='76310288' osm_type='node' osm_id='7096050816' place_rank='22' address_rank='20' boundingbox=\"44.0536113,44.0537113,-0.1769127,-0.1768127\" lat='44.0536613' lon='-0.1768627' display_name='Toulon, Vielle-Soubiran, Mont-de-Marsan, Landes, Nouvelle-Aquitaine, France métropolitaine, 40240, France' class='place' type='isolated_dwelling' importance='0.42'>\n" +
                "<extratags></extratags></place><place place_id='71042684' osm_type='node' osm_id='6457388527' place_rank='22' address_rank='20' boundingbox=\"47.5536926,47.5537926,-1.6251926,-1.6250926\" lat='47.5537426' lon='-1.6251426' display_name='Toulon, Nozay, Châteaubriant-Ancenis, Loire-Atlantique, Pays de la Loire, France métropolitaine, 44170, France' class='place' type='isolated_dwelling' importance='0.42'>\n" +
                "<extratags></extratags></place></searchresults>";
        InputStream anyInputStream = new ByteArrayInputStream(xml.getBytes());
        PowerMockito.mockStatic(AddressInfoBuilder.class);
        Mockito.when(AddressInfoBuilder.connect(Mockito.anyString(), Mockito.<String>anyList())).thenReturn(anyInputStream);
        Mockito.when(AddressInfoBuilder.findTownInseeCode(Mockito.anyString())).thenCallRealMethod();
        Mockito.when(AddressInfoBuilder.getAddressServiceForInseeCode(Mockito.anyString())).thenCallRealMethod();
        String insee = demographicDataService.returnTownInseeCode("Paris");
        assertEquals("83137", insee);
    }



    /**
     * Add entities in Database to test the search on.
     *
     * @param entityManager: EntityManager used to merge entities.
     */
    private static void addTestEntitiesToDB(EntityManager entityManager) {
        for (CountryCode entity : getCountryCodes()) {
            entityManager.merge(entity);
        }
    }

    public void setApplicationMode(String mode){
        ApplicationConfigurationQuery query = new ApplicationConfigurationQuery();
        query.variable().eq("application_mode");
        ApplicationConfiguration application_mode = query.getUniqueResult();
        if(application_mode == null) {
            application_mode = new ApplicationConfiguration();
            application_mode.setVariable("application_mode");
        }
        application_mode.setValue(mode);
        application_mode.save();
    }

    /**
     * Getter for the list of existing entities for tests.
     *
     * @return the list of entities available for Search tests
     */
    private static List<CountryCode> getCountryCodes() {
        List<CountryCode> countryCodes = new ArrayList<>();

        CountryCode france = new CountryCode();
        france.setId(1);
        france.setName("FRANCE");
        france.setIso3("FRA");
        france.setIso("FR");
        france.setCx_4_code("FR");
        france.setPrintableName("France");
        CountryCode belgium = new CountryCode();
        belgium.setId(2);
        belgium.setName("BELGIUM");
        belgium.setIso3("BEL");
        belgium.setIso("BE");
        belgium.setInseeCode("99131");
        belgium.setCx_4_code("BE");
        belgium.setPrintableName("Belgium");
        CountryCode argentina = new CountryCode();
        argentina.setId(3);
        argentina.setName("ARGENTINA");
        argentina.setIso3("ARG");
        argentina.setIso("AR");
        argentina.setInseeCode("99415");
        argentina.setCx_4_code("AR");
        argentina.setPrintableName("Argentina");
        CountryCode china = new CountryCode();
        china.setId(4);
        china.setName("CHINA");
        china.setIso3("CHN");
        china.setIso("CN");
        china.setInseeCode("99216");
        china.setCx_4_code("CN");
        china.setPrintableName("China");

        countryCodes.add(france);
        countryCodes.add(belgium);
        countryCodes.add(argentina);
        countryCodes.add(china);
        return countryCodes;
    }
}
