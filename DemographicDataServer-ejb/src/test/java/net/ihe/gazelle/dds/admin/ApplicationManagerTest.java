package net.ihe.gazelle.dds.admin;

import org.junit.Test;
import org.mockito.Mockito;
import org.testng.Assert;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;

public class ApplicationManagerTest {
    private static final String tosLink = "https://test.net";

    @Test
    public void getCguUrlTest_ok(){
        ApplicationManager applicationManager = mock(ApplicationManager.class);
        Mockito.when(applicationManager.getTosLink()).thenReturn(tosLink);
        Assert.assertEquals(tosLink,"https://test.net");
    }
    @Test
    public void getCguUrlTest_ko(){
        ApplicationManager applicationManager = mock(ApplicationManager.class);
        Mockito.when(applicationManager.getTosLink()).thenReturn(tosLink);
        assertFalse(tosLink.equals("this is test string"));
    }
}
